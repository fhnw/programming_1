package Schnuppertag;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class HelloWorld extends Application {
	public static void main(String[] args) {
	    launch(args);
	  }

	  @Override
	  public void start(Stage primaryStage) {
	    Label label = new Label("Hello, World!");

	    GridPane root = new GridPane();
	    root.add(label, 0, 0);

	    Scene scene = new Scene(root);
	    primaryStage.setScene(scene);
	    primaryStage.show();
	  }
}

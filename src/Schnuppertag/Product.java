package Schnuppertag;

/**
 * Represents a product in a web shop. In a real shop, we would have a variety
 * of products, represented by many related classes
 */
public class Product {
	private static int nextID = 0;
	final int ID = nextID++;
	String name;
	String description;
	int price;
}

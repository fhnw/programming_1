package Schnuppertag;

import java.util.ArrayList;

public class ShoppingCart {
	ArrayList<ShoppingCartItem> items;
	
	public void addItem(Product product, int quantity) {
		ShoppingCartItem newItem = new ShoppingCartItem();
		newItem.productID = product.ID;
		newItem.quantity = quantity;
		newItem.price = product.price;
		items.add(newItem);
	}
	
	public int getTotal() {
		int total = 0;
		for (ShoppingCartItem item : items) {
			total = total + item.quantity * item.price;
		}
		return total;
	}
}

package v2019.TicTacToeProject;

/**
 * Represents a TicTacToe game as an array of ValidMove objects. Where no move
 * has been made, the array entry is null. This class also defines the following
 * methods:
 * 
 * - String gameOver() returns a String with the game result, or null if the
 * game is not over
 * 
 * - ValidMove[][] getBoard() returns a *copy* of the current board
 * 
 * - boolean makeMove(GameMove) makes the given move and returns true (if
 * valid); otherwise does nothing and returns false
 */
public class GameLogic {
	private ValidMove[][] board = new ValidMove[3][3];

	/** Default constructor, shown for completeness */
	public GameLogic() {
	}

	/**
	 * Return a read-only copy of the game board
	 */
	public ValidMove[][] getBoard() {
		return null; // TODO
	}

	/**
	 * Check columns, rows and diagonals for three identical symbols. If no winner found, check for draw.
	 */
	public String getGameResult() {
		return null; // TODO
	}

	/**
	 * Place the given move onto the board
	 * 
	 * @return true if move is valid, false if move cannot be made
	 */
	public boolean makeMove(GameMove move) {
		return false; // TODO
	}
}

package v2019.TicTacToeProject;

/**
 * Defines the moves that are possible
 */
public enum ValidMove {
	X, O
}

package v2019.topic10_MulticlassPrograms.interfaces.fraction;

public interface MathFunctions {
	public Fraction add(Fraction n);
	public Fraction subtract(Fraction n);
	public Fraction multiply(Fraction n);
	public Fraction divide(Fraction n);
}

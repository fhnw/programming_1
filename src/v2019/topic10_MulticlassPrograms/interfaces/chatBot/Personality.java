package v2019.topic10_MulticlassPrograms.interfaces.chatBot;

public interface Personality {
	public abstract String getGreeting();
	public abstract String answerQuestion();
	public abstract String getFarewell();
	public abstract String getOther();
}

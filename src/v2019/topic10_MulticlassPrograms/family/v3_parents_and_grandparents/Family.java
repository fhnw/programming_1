package v2019.topic10_MulticlassPrograms.family.v3_parents_and_grandparents;

import java.util.ArrayList;
import java.util.Scanner;
public class Family {
	private static Family mainProgram;
	private ArrayList<Person> familyMembers;

	public static void main(String[] args) {
		mainProgram = new Family();
		mainProgram.start();
	}

	private Family() {
		familyMembers = new ArrayList<>();
	}

	private void start() {
		Scanner in = new Scanner(System.in);
		printCommandMenu();
		String command = in.nextLine();
		while (!command.equals("stop")) {
			runCommand(in, command);
			printCommandMenu();
			command = in.nextLine();
		}
	}

	private void printCommandMenu() {
		System.out.println("Available commands:");
		System.out.println("  stop - end the program");
		System.out.println("  add <first name> <last name> - add new person to the family");
		System.out.println("  grandparents <ID> - display person's grandparents");
		System.out.println("  all - list all people in the database");
		System.out.println("Enter a command:");
	}

	private void runCommand(Scanner in, String command) {
		if (command.startsWith("add")) {
			add(in, command);
		} else if (command.equals("all")) {
			printAll();
		} else if (command.startsWith("grandparents")) {
			getGrandparents(command);
		} else {
			System.out.println("Unknown command");
		}
	}

	/**
	 * We expect the command to contain three parts: add firstName lastName
	 * We then read additional information from the command line
	 */
	private void add(Scanner in, String command) {
		String[] parts = command.split(" ");
		System.out.println("Enter the gender: m for male, anything else for female");
		String strGender = in.nextLine();
		Person.Gender gender = (strGender.charAt(0) == 'm') ? Person.Gender.MALE : Person.Gender.FEMALE;
		Person newPerson = new Person(parts[2], parts[1], gender);
		
		System.out.println("Enter the ID of the father, or 'none'");
		String strFather = in.nextLine();
		if (!strFather.startsWith("none")) {
			int idFather = Integer.parseInt(strFather);
			Person father = Person.findPerson(familyMembers, idFather);
			if (father != null) {
				newPerson.setFather(father);
				father.addChild(newPerson);
			}
		}
		
		System.out.println("Enter the ID of the mother, or 'none'");
		String strMother = in.nextLine();
		if (!strMother.startsWith("none")) {
			int idMother = Integer.parseInt(strMother);
			Person mother = Person.findPerson(familyMembers, idMother);
			if (mother != null) {
				newPerson.setFather(mother);
				mother.addChild(newPerson);
			}
		}
		
		familyMembers.add(newPerson);
	}

	private void printAll() {
		System.out.println("- - - - -");
		for (Person p : familyMembers) System.out.println(p);
		System.out.println("- - - - -");
	}

	/**
	 * We expect the command to contain two parts: grandparents ID
	 * The ID should be the ID of an existing person. We then just
	 * call the getGrandparents method in class Person
	 */
	private void getGrandparents(String command) {
		String[] parts = command.split(" ");
		int ID = Integer.parseInt(parts[1]);
		Person p = Person.findPerson(familyMembers, ID);
		if (p != null) {
			ArrayList<Person> grandparents = p.getGrandparents();
			System.out.println("- - - - -");
			for (Person gp : grandparents) System.out.println(gp);
			System.out.println("- - - - -");
		}
	}
}

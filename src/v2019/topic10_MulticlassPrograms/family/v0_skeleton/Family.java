package v2019.topic10_MulticlassPrograms.family.v0_skeleton;

import java.util.ArrayList;
import java.util.Scanner;

public class Family {
	private static Family mainProgram;
	private ArrayList<Person> familyMembers;

	public static void main(String[] args) {
		mainProgram = new Family();
		mainProgram.start();
	}
	
	private Family() {
		familyMembers = new ArrayList<>();
	}

	private void start() {
		Scanner in = new Scanner(System.in);
		printCommandMenu();
		String command = in.nextLine();
		while (!command.equals("stop")) {
			runCommand(command);
			printCommandMenu();
			command = in.nextLine();
		}
		in.close();
	}
	
	private void printCommandMenu() {
		System.out.println("Enter a command:");
	}
	
	private void runCommand(String command) {
		// TODO
	}
}

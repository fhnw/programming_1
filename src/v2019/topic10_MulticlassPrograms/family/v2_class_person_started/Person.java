package v2019.topic10_MulticlassPrograms.family.v2_class_person_started;

import java.util.ArrayList;

public class Person {
	private static int highestID = -1;
	
	private final int ID;
	private String lastName;
	private String firstName;
	private Gender gender;
	private Person father;
	private Person mother;
	private ArrayList<Person> children;
	
	public enum Gender { MALE, FEMALE };

	// Class method to get the next available ID
	  private static int getNextID() {
	    highestID++;
	    return highestID;
	  }
	  
	private Person() {
		this.ID = getNextID();
		children = new ArrayList<>();
	}
	
	public Person(String lastName, String firstName, Gender gender) {
		this(); // Call to constructor with no parameters
		this.lastName = lastName;
		this.firstName = firstName;
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return "ID " + ID + ": " + firstName + " " + lastName;
	}

	// --- Getters and Setters ---
	
	public int getID() {
		return ID;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (!lastName.isEmpty()) this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (!firstName.isEmpty()) this.firstName = firstName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Person getFather() {
		return father;
	}

	public void setFather(Person father) {
		this.father = father;
	}

	public Person getMother() {
		return mother;
	}

	public void setMother(Person mother) {
		this.mother = mother;
	}

	public ArrayList<Person> getChildren() {
		return children;
	}

	public void addChild(Person child) {
		if (!children.contains(child)) children.add(child);
	}
}

package v2019.topic10_MulticlassPrograms.family.v1_gui_complete;

import java.util.ArrayList;
import java.util.Scanner;

public class Family {
	private static Family mainProgram;
	private ArrayList<Person> familyMembers;

	public static void main(String[] args) {
		mainProgram = new Family();
		mainProgram.start();
	}

	private Family() {
		familyMembers = new ArrayList<>();
	}

	private void start() {
		Scanner in = new Scanner(System.in);
		printCommandMenu();
		String command = in.nextLine();
		while (!command.equals("stop")) {
			runCommand(command);
			printCommandMenu();
			command = in.nextLine();
		}
		in.close();
	}

	private void printCommandMenu() {
		System.out.println("Available commands:");
		System.out.println("  stop - end the program");
		System.out.println("  add <first name> <last name> - add new person to the family");
		System.out.println("  grandparents <ID> - display person's grandparents");
		System.out.println("  all - list all people in the database");
		System.out.println("Enter a command:");
	}

	private void runCommand(String command) {
		if (command.startsWith("add")) {
			add(command);
		} else if (command.equals("all")) {
			printAll();
		} else if (command.startsWith("grandparents")) {
			getGrandparents(command);
		} else {
			System.out.println("Unknown command");
		}
	}

	private void add(String command) {
		System.out.println("TODO: add person");
	}

	private void printAll() {
		System.out.println("TODO: printAll");
	}

	private void getGrandparents(String command) {
		System.out.println("TODO: get grandparents");
	}
}

package v2019.topic13_Exceptions_DataValidation;

public class LineItem {
	private int ID;
	private int productID;
	private int quantity;
	private int priceInRappen;
	
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getPriceInRappen() {
		return priceInRappen;
	}
	public void setPriceInRappen(int priceInRappen) {
		this.priceInRappen = priceInRappen;
	}
	public void setID(int iD) {
		ID = iD;
	}	
}

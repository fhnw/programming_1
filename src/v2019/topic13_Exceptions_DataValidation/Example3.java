package v2019.topic13_Exceptions_DataValidation;

import java.util.Scanner;

public class Example3 {

	public static void main(String[] args) {
		int userValue = readUserInput();
		System.out.print(userValue);

	}

	public static int readUserInput() {
		Scanner in = new Scanner(System.in);
		int value = -1;
		boolean goodInput = false;
		while (!goodInput) {
			System.out.println("Please enter your age");
			try {
				String inStr = in.nextLine();
				value = Integer.parseInt(inStr); // may throw NumberFormatException
				if (value < 0 | value > 100) {
					System.out.println("That's not really your age - try again");
				} else {
					goodInput = true;
				}
			} catch (NumberFormatException e) {
				System.out.println("That was not an integer - try again");
			}
		}
		in.close();
		return value;
	}
}

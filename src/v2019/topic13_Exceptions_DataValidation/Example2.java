package v2019.topic13_Exceptions_DataValidation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Example2 {

	public static void main(String[] args) {
		File file = new File("MyFile.txt");
		try {
			FileReader reader = new FileReader(file);
			// process the file here
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
		} catch (IOException e) {
			System.out.println("Error processing file!");
		}
	}
}

package v2019.topic13_Exceptions_DataValidation;

import java.util.Scanner;

public class Example1 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter an integer - if you don't, I die...");
		int x = in.nextInt();
		System.out.println("You entered " + x);
		in.close();
	}

}

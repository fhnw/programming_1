package v2019.topic13_Exceptions_DataValidation;

import java.time.LocalDate;
import java.util.ArrayList;

public class Invoice {
	private int ID;
	private LocalDate date;
	private int CustomerID;
	private ArrayList<LineItem> lineItems;
	private String note;
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public int getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(int customerID) {
		CustomerID = customerID;
	}
	public ArrayList<LineItem> getLineItems() {
		return lineItems;
	}
	public void setLineItems(ArrayList<LineItem> lineItems) {
		this.lineItems = lineItems;
	}
	public int getID() {
		return ID;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
}

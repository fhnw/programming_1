package v2019.topic13_Exceptions_DataValidation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Example1_Fixed {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean goodInput = false;
		int x = 0;
		while (!goodInput) {
			try {
				System.out.println("Please enter an integer: ");
				x = in.nextInt();
				goodInput = true;
			} catch (InputMismatchException e) {
				System.out.println("Bad user, no cookie!");
				in.nextLine();
			}
		}
		System.out.println("You entered " + x);
		in.close();
	}
}

package v2019.topic08_JavaFX_Intro.drawing;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class SolarSystem extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Group shapes = new Group();
		
		Circle earthOrbit = new Circle(150, 150, 90, Color.BLACK);
		earthOrbit.setStroke(Color.LIGHTBLUE);
		shapes.getChildren().add(earthOrbit);
		Circle earth = new Circle(212, 212, 8, Color.LIGHTBLUE);
		shapes.getChildren().add(earth);
		
		Circle venusOrbit = new Circle(150, 150, 60, Color.BLACK);
		venusOrbit.setStroke(Color.LIGHTGREEN);
		shapes.getChildren().add(venusOrbit);
		Circle venus = new Circle(210, 150, 6, Color.LIGHTGREEN);
		shapes.getChildren().add(venus);
		
		Circle mercuryOrbit = new Circle(150, 150, 30, Color.BLACK);
		mercuryOrbit.setStroke(Color.PINK);
		shapes.getChildren().add(mercuryOrbit);
		Circle mercury = new Circle(150, 180, 3, Color.PINK);
		shapes.getChildren().add(mercury);

		Circle sun = new Circle(150, 150, 15, Color.YELLOW);
		shapes.getChildren().add(sun);
		
		// Create the scene
		Scene scene = new Scene(shapes, 300, 300, Color.BLACK);
		primaryStage.setTitle("Inner solar system");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}

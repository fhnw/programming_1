package v2019.topic08_JavaFX_Intro.drawing;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class DrawShapes extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Group shapes = new Group();
		
		Circle circle = new Circle(60, 100, 40, Color.LIGHTBLUE);
		circle.setStroke(Color.DARKBLUE);
		shapes.getChildren().add(circle);

		Ellipse ellipse = new Ellipse(120, 60, 40, 30);
		ellipse.setFill(Color.ORANGE);
		ellipse.setStroke(Color.GREEN);
		shapes.getChildren().add(ellipse);
		
		Rectangle rect = new Rectangle(80, 70, 70, 50);
		rect.setFill(Color.YELLOW);
		rect.setStroke(Color.RED);
		shapes.getChildren().add(rect);

		// Create the scene
		Scene scene = new Scene(shapes);
		primaryStage.setTitle("Shapes in JavaFX");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}

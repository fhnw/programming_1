package v2019.topic12_AdvancedClassDesign.astronomy;

/**
 * A class to represent stars, planets and moons...
 * 
 * @author Brad
 *
 */
public abstract class AstroBody implements Comparable<AstroBody> {
	public static final double G = 6.67428E-11; // Gravitational constant
	private final String name; // Catalog number or unique name
	protected double diameter; // in meters
	protected double mass; // in kg
	private AstroBody orbiting; // what object this object orbits (null if n/a)

	public AstroBody(String name) {
		this.name = name;
		diameter = 0;
		mass = 0;
		orbiting = null;
	}
	
	public abstract double getSurfaceGravity();

	@Override
	public int compareTo(AstroBody o) {
		if (this.getMass() < o.getMass()) {
			return -1;
		} else if (this.getMass() > o.getMass()) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) return false;
		AstroBody other = (AstroBody) obj;
		if (name == null) 
			return (other.name == null);
		else
			return (name.equals(other.name));
	}

	public String toString() {
		return this.name;
	}

	// Getters and setters...

	public String getName() {
		return name;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public double getMass() {
		return mass;
	}

	public final void setOrbiting(AstroBody orbiting) {
		this.orbiting = orbiting;
	}

	public final AstroBody getOrbiting() {
		return orbiting;
	}
}

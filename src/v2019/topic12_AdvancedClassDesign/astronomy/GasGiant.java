package v2019.topic12_AdvancedClassDesign.astronomy;

public class GasGiant extends Planet {
	private double coreMass;
	private double coreDiameter;

	public GasGiant(String name) {
		super(name);
	}

	/**
	 * Gravity for a gas giant is at the core, ignoring atmosphere
	 */
	@Override
	public double getSurfaceGravity() {
		return G * coreMass / (coreDiameter * coreDiameter / 4);
	}

	public void setCoreDiameter(double coreDiameter) {
		this.coreDiameter = coreDiameter;
	}

	public double getCoreDiameter() {
		return coreDiameter;
	}

	public void setCoreMass(double coreMass) {
		this.coreMass = coreMass;
	}

	public double getCoreMass() {
		return coreMass;
	}
}

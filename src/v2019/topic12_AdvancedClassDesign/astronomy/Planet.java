package v2019.topic12_AdvancedClassDesign.astronomy;

import java.util.ArrayList;

public class Planet extends AstroBody {
	public static enum Gas {
		HYDROGEN, HELIUM, OXYGEN, NITROGEN, METHANE
	};

	private ArrayList<Planet> moons;
	private ArrayList<Gas> atmosphericGases;

	public Planet(String name) {
		super(name);
		atmosphericGases = new ArrayList<Gas>();
	}

	/**
	 * Calculate surface gravity from mass and diameter
	 */
	@Override
	public double getSurfaceGravity() {
		return G * mass / (diameter * diameter / 4);
	}

	public void addMoon(Planet p) {
		moons.add(p);
	}

	public ArrayList<Planet> getMoons() {
		return moons;
	}

	public void addAtmospericGas(Gas g) {
		atmosphericGases.add(g);
	}

	public ArrayList<Gas> getAtmosphericGases() {
		return atmosphericGases;
	}
}

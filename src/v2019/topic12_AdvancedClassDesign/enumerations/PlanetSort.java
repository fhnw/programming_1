package v2019.topic12_AdvancedClassDesign.enumerations;

import java.util.ArrayList;

/**
 * First, construct a list of the planets, directly from the enumeration.
 * 
 * Then, sort into a new list, using the planet's gravity as the sort attribute.
 * 
 * This just to practice using object-methods from the enumeration
 */
public class PlanetSort {

	public static void main(String[] args) {
		// Construct unsorted list
		ArrayList<Planet> unsortedList = new ArrayList<>();
		for (Planet p : Planet.values()) {
			unsortedList.add(p);
		}

		// Create a sorted list
		ArrayList<Planet> sortedList = selectionSort(unsortedList);

		// Print out the planets and their gravity
		for (Planet p : sortedList) {
			Double gravity = p.surfaceGravity();
			System.out.println("Surface gravity on " + p.toString() + " is " + gravity);
		}
	}

	/**
	 * A rather dumb way to sort - selection sort into a new list
	 */
	private static ArrayList<Planet> selectionSort(ArrayList<Planet> unsortedList) {
		ArrayList<Planet> sortedList = new ArrayList<>();

		while (unsortedList.size() > 0) {
			// Find planet with lightest gravity
			Planet lightest = unsortedList.get(0);
			for (Planet p : unsortedList) {
				if (p.surfaceGravity() < lightest.surfaceGravity()) lightest = p;
			}

			// Remove from unsorted list
			unsortedList.remove(lightest);

			// Add to end of sorted list
			sortedList.add(lightest);
		}

		return sortedList;
	}

}

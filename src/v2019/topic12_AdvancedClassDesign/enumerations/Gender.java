package v2019.topic12_AdvancedClassDesign.enumerations;

public enum Gender {
	MALE("Herr", "Sehr geehrter"),  // Sehr geehrter Herr X
	FEMALE("Frau", "Sehr geehrte"), // Sehr geehrte Frau Y
	COUPLE("Familie", "Sehr geehrte Familie"); // Sehr geehrte Familie Z
	
	private final String title;
	private final String opening;
	
	private Gender(String title, String opening) {
		this.title = title;
		this.opening = opening;
	}

	public String getTitle() {
		return title;
	}

	public String getOpening() {
		return opening;
	}
}

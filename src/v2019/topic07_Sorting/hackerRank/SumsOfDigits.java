package v2019.topic07_Sorting.hackerRank;

import java.util.Arrays;
import java.util.Scanner;

public class SumsOfDigits {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numValues = in.nextInt();
		int[] valuesIn = new int[numValues];
		for (int i = 0; i < numValues; i++) {
			valuesIn[i] = in.nextInt();
		}
		in.close();
		
		int[] valuesOut = sumsOfDigits(valuesIn);
		
		for (int value : valuesOut) System.out.println(value);
	}
	
	static int[] sumsOfDigits(int[] valuesIn) {
		// Overwrite the values with the sums of their digits
		for (int i = 0; i < valuesIn.length; i++) {
			valuesIn[i] = sumOfDigits(valuesIn[i]);
		}
			
		// Sort the array
		Arrays.sort(valuesIn);
			
		// Count the unique values,
		// create an output array of the right size
		int numUnique = countUnique(valuesIn);
		int[] valuesOut = new int[numUnique];
			
		// Copy the values into the output array
		copyValues(valuesIn, valuesOut);
		return valuesOut;
	}
	
	static int sumOfDigits(int valueIn) {
		int sum = 0;
		while (valueIn > 0) {
			sum += valueIn % 10;
			valueIn = valueIn / 10;
		}
		return sum;
    }
	
	/**
	 * Count the unique values in an array. We assume that the
	 * values are sorted: once we have seen a value, we skip over
	 * all values that are the same. Since all values are >= 0,
	 * we know that a negative value is invalid.
	 */
	static int countUnique(int[] values) {
		int valueSeen = -1;
		int numDifferentValues = 0;
		for (int value : values) {
			if (value != valueSeen) {
				numDifferentValues++;
				valueSeen = value;
			}
		}
		return numDifferentValues;
	}
	
	/**
	 * Copy the unique values into the output array. Very similar
	 * to "countUnique".
	 */
	static void copyValues(int[] values, int[] valuesOut) {
		int valueSeen = -1;
		int valueIndex = -1;
		for (int value : values) {
			if (value != valueSeen) {
				valuesOut[++valueIndex] = value;
				valueSeen = value;
			}
		}
	}	
}

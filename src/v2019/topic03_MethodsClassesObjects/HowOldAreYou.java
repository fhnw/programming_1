package v2019.topic03_MethodsClassesObjects;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class HowOldAreYou {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter year, month and day of your birth date (integers):");
		int year = in.nextInt();
		int month = in.nextInt();
		int day = in.nextInt();
		in.close();
		
		LocalDate birthDate = LocalDate.of(year, month, day);
		LocalDate today = LocalDate.now();
		
		Period result = Period.between(birthDate, today);
		
		int years = result.getYears();
		int months = result.getMonths();
		int days = result.getDays();
		
		System.out.println("Your age is: " + years + " years, " + months + " months, and " + days + " days");
		
		// Alternate approach
		long numDays = birthDate.until(today, ChronoUnit.DAYS);
		System.out.print("Total days = " + numDays);

	}
}

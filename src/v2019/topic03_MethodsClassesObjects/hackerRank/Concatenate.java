package v2019.topic03_MethodsClassesObjects.hackerRank;

import java.util.Scanner;

public class Concatenate {

	public static void main(String[] args) {
		String s1 = "This is the story of ";
		String s2 = new String("Some people called ");
		
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a grand-sounding name, like 'Alexander the Great'");
		String name = in.next(); // Get a single word
		String restOfLine = in.nextLine(); // Get the rest of the line
		in.close();
		
		// TODO: Replace the empty strings ("") to produce the required output
		String result1 = "";
		String result2 = "";
		
		// Print the output
		System.out.println(result1);
		System.out.println(result2);
	}
}

package v2019.topic03_MethodsClassesObjects.hackerRank;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class DaysBetweenDates {

	public static void main(String[] args) {
		// Create scanner, prompt user
		Scanner in = new Scanner(System.in);
		System.out.println("Enter two dates: ");
		
		// Read in two dates (year, month, day as integers)

		// Calculate number of days using the LocalDate.until method
		
		// Print the number of days
	}
}

package v2019.topic03_MethodsClassesObjects.hackerRank;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Scanner;

public class SecondsToday {

	public static void main(String[] args) {
		// Create scanner, prompt user
		Scanner in = new Scanner(System.in);
		System.out.println("Enter time of day (hours, minutes, seconds): ");
		
		// Read in hours, minutes, seconds - create LocalTime object

		// Calculate number of seconds past, using Duration class
		
		// Print the number of seconds
	}
}

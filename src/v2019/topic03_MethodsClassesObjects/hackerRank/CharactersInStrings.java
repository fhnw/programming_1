package v2019.topic03_MethodsClassesObjects.hackerRank;

import java.util.Scanner;

public class CharactersInStrings {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a string containing at least one character");
		String s = in.nextLine();
		in.close();

		// Print the location of the first 'c' in the string.
		// If there is no 'c', print "none"
		
		// Print the center character in the string. If the string has an even
		// number of characters, print the character to the *left* of center.
	}
}

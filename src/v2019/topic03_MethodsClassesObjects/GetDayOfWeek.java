package v2019.topic03_MethodsClassesObjects;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class GetDayOfWeek {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter year, month and day as integers:");
		int year = in.nextInt();
		int month = in.nextInt();
		int day = in.nextInt();
		in.close();
		
		LocalDate when = LocalDate.of(year, month, day);
		
		DayOfWeek dow = when.getDayOfWeek(); // This is also a class in java.time
		
		DateTimeFormatter myFormat = DateTimeFormatter.ofPattern("dd MMM YYYY");
		
		String result = when.format(myFormat) + " was on a " + dow.toString();
		
		System.out.println(result);
	}
}

package v2019.topic03_MethodsClassesObjects.dice;

import java.util.Scanner;

public class Dice_v0 {

	public static void main(String[] args) {
		int sizeOfDice = readUserInput("Size of dice", 2, 100);
		int numberOfDice = readUserInput("Number of dice", 1, 1000);
		int totalAllRolls = simulateDice(sizeOfDice, numberOfDice);
		float average = calculateAverage(totalAllRolls, numberOfDice);
		printOutput(average, sizeOfDice);
	}

	private static int readUserInput(String prompt, int min, int max) {
		return 0;
	}
	
	private static int simulateDice(int dieSize, int numDice) {
		return 0;
	}
	
	private static float calculateAverage(int totalAllRolls, int numberOfDice) {
		return 0;
	}
	
	private static void printOutput(float average, int sizeOfDice) {
		float theoreticalAverage = (1.0f + (float) sizeOfDice) / 2.0f;
		System.out.println("Average should be " + theoreticalAverage);
	}
}

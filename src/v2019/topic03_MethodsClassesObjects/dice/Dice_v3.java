package v2019.topic03_MethodsClassesObjects.dice;

import java.util.Scanner;

public class Dice_v3 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int sizeOfDice = readUserInput(in, "Size of dice", 2, 100);
		int numberOfDice = readUserInput(in, "Number of dice", 1, 1000);
		in.close();
		
		int totalAllRolls = simulateDice(sizeOfDice, numberOfDice);
		float average = calculateAverage(totalAllRolls, numberOfDice);
		printOutput(average, sizeOfDice);
	}

	/**
	 * This method prints a prompt, and then reads a single integer from the
	 * user. If the value is not in the allowed range, it asks the user to try
	 * again.
	 * 
	 * @param prompt The prompt to display
	 * @param min The minimum allowed value
	 * @param max The maximum allowed value
	 * @return The value entered by the user
	 */
	private static int readUserInput(Scanner in, String prompt, int min, int max) {
		System.out.println(prompt);
		int value = in.nextInt();
		while (value < min || value > max) {
			System.out.println("Value must be between " + min + " and " + max + " - try again");
			value = in.nextInt();
		}
		return value;
	}
	
	/**
	 * Roll the dice, and accumulate the sum of all rolls
	 * 
	 * @param dieSize The number of sides per die
	 * @param numDice The number of dice to roll
	 * @return The total of all rolls
	 */
	private static int simulateDice(int dieSize, int numDice) {
		int sum = 0;
		for (int i = 0; i < numDice; i++) {
			int dieValue = (int) (Math.random() * dieSize + 1);
			sum += dieValue;
		}
		return sum;
	}

	/**
	 * Calculate the average of the dice rolls. This is really too small
	 * to be a method, but this is just an example...
	 * 
	 * @param totalAllRolls
	 * @param numberOfDice
	 * @return average
	 */
	private static float calculateAverage(int totalAllRolls, int numberOfDice) {
		// Must convert to float before calculating (do you know why?)
		return (float) totalAllRolls / (float) numberOfDice;
	}
	
	private static void printOutput(float average, int sizeOfDice) {
		float theoreticalAverage = (1.0f + (float) sizeOfDice) / 2.0f;
		System.out.println("Average should be " + theoreticalAverage);
		System.out.println("Average actually is " + average);
	}
}

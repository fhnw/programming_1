package v2019.topic14_examPrep.solutions.poker;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class represents a deck of playing cards. When initially created, the deck is filled and shuffled.
 * Later, the deck can be refilled and reshuffled by calling the "shuffle" method.
 */
public class DeckOfCards {
    private final ArrayList<Card> cards = new ArrayList<>();

    /**
     * We only ever have one deck of cards, so we do not set an ID attribute.
     */
    public DeckOfCards() {
        shuffle();
    }

    /**
     * How many cards are left in the deck?
     */
    public int getCardsRemaining() {
        return cards.size();
    }

    /**
     * Gather all 52 cards, and shuffle them
     */
    public void shuffle() {
        // Remove all cards
        cards.clear();
        
        // Add all 52 cards
        for (Card.Suit suit : Card.Suit.values()) {
            for (Card.Rank rank : Card.Rank.values()) {
                Card card = new Card(suit, rank);
                cards.add(card);
            }
        }
        
        // Shuffle
        Collections.shuffle(cards);
    }

    /**
     * Take one card from the deck and return it
     * 
     * This is an example of conditional assignment
     */
    public Card dealCard() {
        return (cards.size() > 0) ? cards.remove(cards.size()-1) : null;
    }
}

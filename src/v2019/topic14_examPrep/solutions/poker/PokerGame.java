package v2019.topic14_examPrep.solutions.poker;

import java.util.Optional;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class PokerGame extends Application implements EventHandler<ActionEvent> {
    private PlayerPane player1 = new PlayerPane("Player 1");
    private PlayerPane player2 = new PlayerPane("Player 2");
    private Label lblDeck = new Label("");
    Region spacer = new Region(); // Empty spacer in the control area
    private Button btnShuffle = new Button("Shuffle");
    private Button btnDeal = new Button("Deal");
    private Button btnQuit = new Button("Quit");
    
    DeckOfCards deck = new DeckOfCards();

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Create basic layout
        HBox playerBox = new HBox(player1, player2);
        HBox controlBox = new HBox(5, lblDeck, spacer, btnShuffle, btnDeal, btnQuit);
        VBox root = new VBox(playerBox, controlBox);
        
        // Format control area
        HBox.setHgrow(spacer, Priority.ALWAYS); // Use region for spacing
        controlBox.setId("controlArea"); // Unique ID in the CSS
        updateDeckDisplay(); // update the deck display
        
        // Register for button events
        btnShuffle.setOnAction(this);
        btnDeal.setOnAction(this);
        btnQuit.setOnAction(this);
        
        // Handle window closing with a dialog
        primaryStage.setOnCloseRequest( e -> {
                Alert alert = new Alert(AlertType.ERROR, "Click 'Quit' to close the window");
                alert.showAndWait();
                e.consume();
        });

        // Create the scene using our layout; then display it
        Scene scene = new Scene(root);
        scene.getStylesheets().add(
                getClass().getResource("poker.css").toExternalForm());
        primaryStage.setTitle("Poker v2");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Handle button clicks: we can do this by implementing an interface as well
     */
    @Override
    public void handle(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        if (btnClicked == btnQuit) {
            Platform.exit();
        } else if (btnClicked == btnShuffle) {
            shuffleCards();
        } else if (btnClicked == btnDeal) {
            playPoker();
        }
    }

    /**
     * Remove all cards from players hands, and shuffle the deck
     */
    private void shuffleCards() {
        player1.emptyHand();
        player2.emptyHand();
        deck.shuffle();
        updateDeckDisplay();
    }
    
    /**
     * Deal each player five cards, then evaluate the two hands
     */
    private void playPoker() {
        player1.emptyHand();
        player2.emptyHand();
        if (deck.getCardsRemaining() >= 10) {
            for (int i = 0; i < 5; i++) {
                Card card = deck.dealCard();
                player1.addCard(card);
                
                card = deck.dealCard();
                player2.addCard(card);
                
                updateDeckDisplay();
            }
            
            player1.evaluate();
            player2.evaluate();
        } else {
            Alert alert = new Alert(AlertType.ERROR, "Not enough cards - shuffle first");
            alert.showAndWait();
        }
    }
    
    private void updateDeckDisplay() {
        lblDeck.setText(deck.getCardsRemaining() + " cards in deck");
    }
}

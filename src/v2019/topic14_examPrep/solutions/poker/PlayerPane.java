package v2019.topic14_examPrep.solutions.poker;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.Node;

public class PlayerPane extends VBox {
    private Label lblName = new Label();
    private HBox hboxCards = new HBox();
    private Label lblEvaluation = new Label("--");
    private Label lblWinLoss = new Label("--");
    
    private PokerHand playerHand;
    
    public PlayerPane(String name) {
        super(); // Always call super-constructor first !!
        
        // Add child nodes
        this.getChildren().addAll(lblName, hboxCards, lblEvaluation, lblWinLoss);
        
        // Display empty labels for cards; assign class ID for CSS
        for (int i = 0; i < 5; i++) {
            Label lblCard = new Label();
            lblCard.getStyleClass().add("card");
            hboxCards.getChildren().add(lblCard);
        }
        
        // Set initial display values
        lblName.setText(name);
        
        // Create our PlayerHand
        playerHand = new PokerHand(name);
        
        // Set CSS class name for this Player-VBox
        this.getStyleClass().add("player");
    }
    
    public void emptyHand() {
        // Remove cards from hand
        playerHand.discardHand();
        
        // Clear the display
        for (Node node : hboxCards.getChildren()) {
            // An HBox can contain anything - so it hands us "nodes"
            // But we know that these are really Labels
            Label cardLabel = (Label) node;
            cardLabel.setText("");            
        }
    }
    
    public void addCard(Card card) {
        // Add card to the hand
        playerHand.addCard(card);
        
        // Determine which label this is (index from 0 to 4)
        int index = playerHand.getNumCards() - 1;
        
        // Get the label from the HBox, and update it
        Label cardLabel = (Label) hboxCards.getChildren().get(index);
        cardLabel.setText(card.toString());
    }
    
    public void evaluate() {
        lblEvaluation.setText(playerHand.evaluateHand().toString());
    }
}

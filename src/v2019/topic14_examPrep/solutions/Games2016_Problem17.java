package v2019.topic14_examPrep.solutions;

import java.util.ArrayList;

/**
 * Math and Logic Games, 2016, Quarterfinal problem 17
 * 
 * http://www.smasv.ch/de/
 * 
 * Matthias adds the cubes of the digits of the number 2016. He gets 225. He repeats this with the
 * digits of the result and gets 141, repeating again, he gets 66, 432, 99, 1458, 702, 351, 153,
 * 153... The following numbers are all 153. Use this same procedure for all years in the 21st
 * century, including 2001 and 2100. For how many years does one get the final result 153? *
 */
public class Games2016_Problem17 {
    public static void main(String[] args) {
        int count = 0;
        for (int year = 2001; year <= 2100; year++) {
            if (processNumber(year) == 153) {
                System.out.print(year + " ");
                count++;
            }
        }
        System.out.println("\nTotal found: " + count);
    }
    
    /**
     * Take the incoming integer, and repeat the process (sumCubesOfDigits) until a repeating number is found.
     * Return the first repeating number. In order to detect repetitions, we store all results in an ArrayList,
     * and search the ArrayList with each new result.
     */
    public static int processNumber(int in) {
        ArrayList<Integer> results = new ArrayList<>();
        int result = in;
        do {
            results.add(result);
            result = sumCubesOfDigits(result);
        } while(!results.contains(result));
        return result;
    }

    /**
     * Break the incoming integer into individual digits. Calculate the cube of each digit, and add
     * up the results.
     * 
     * For example: 243: 2^3 = 8, 4^3 = 64, 3^3 = 27. The sum is 99.
     */
    public static int sumCubesOfDigits(int in) {
        int sum = 0;
        while (in > 0) {
            int digit = in % 10;
            in = in / 10;
            sum += digit * digit * digit;
        }
        return sum;
    }

}

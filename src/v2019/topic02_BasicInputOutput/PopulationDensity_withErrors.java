package v2019.topic02_BasicInputOutput;

import java.util.Scanner;


public class PopulationDensity_withErrors {

	/**
	 * Calculate the population density of an area. The area can be square or oval.
	 * If the area is square, the size is width time height. If it is oval,
	 * the area is pi * major-radius * minor-radius.
	 * 
	 * Then read in the population. Calculate the population density as
	 * population density as people per square kilometer.
	 * 
	 * THIS PROGRAM CONTAINS ERRORS - find and fix the errors
	 */
	public static void main(String[] args) {
		double area;
		double answer;
		Scanner userInput = new Scanner(System.in);
		
		// Rectangle or oval?
		System.out.println("If the area is an oval, enter '1'; otherwise it will be a rectangle: ");
		int shape = userInput.nextInt();
		
		// Read width and height (or major/minor radius)
		System.out.println("Enter the width (or major radius): ");
		int x = userInput.nextInt();
		System.out.println("Enter the height (or minor radius): ");
		int y = userInput.nextInt();
		System.out.println("Enter the population: ");
		int population = userInput.nextInt();
		
		userInput.close(); // done reading from user
		
		if (shape == 1) { // ellipse
			area = x * y * Math.PI;
		} else { // rectangle
			area = x * x;
		}

		answer = area / population;
		System.out.println("Population density: " + answer);
	}

}

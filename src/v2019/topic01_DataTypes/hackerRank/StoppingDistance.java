package v2019.topic01_DataTypes.hackerRank;

public class StoppingDistance {

	public static void main(String[] args) {
	    // These variables are floats. They could also be doubles.
		// Why should they *not* be integers?
	    float velocity = 120.0f; // 120 km/h
	    float reactionTime = 0.3f; // in seconds
	    
	    /* Enter your code here. */
	}

}

package v2019.topic01_DataTypes;

public class TestFloatingPoint {

	/**
	 * The floating point representation can represent fractions, including very
	 * small numbers. It can also represent very large numbers. However, there are
	 * gaps in the representation: very small gaps near zero, and larger gaps as the
	 * numbers get very large. The program below tests floating point numbers
	 * represented by the primitive data type float. Specifically, it searches for
	 * the first (lowest) gap that is greater than or equal to 100.
	 * 
	 * As it happens, this gap is between 1,073,741,824 and 1,073,741,952; in other
	 * words, the float data type cannot represent any of the values from
	 * 1,073,741,825 through 1,073,741,951.
	 */
	public static void main(String[] args) {
		long lastExactValue = 0;
		float f;
		long i, j;
		for (i = 1; i < Long.MAX_VALUE; i++) {
			// Convert the long to a float
			f = i;

			// Convert back to long
			j = (long) f;

			// if this value was represented exactly by a float
			if (j == i) {
				// Have we found a gap of 100 or more?
				if ((i - lastExactValue) >= 100) {
					System.out.println("Gap found: " + lastExactValue + " - " + i);
					break;
				}
				lastExactValue = i;
			}
		}
	}
}

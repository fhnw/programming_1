package v2019.topic01_DataTypes;

public class interest {

	public static void main(String[] args) {
		double amount = 103.55;
		double interestRate = 1.2;
		
		amount = amount + amount * interestRate / 100;
		System.out.println("After 1 year " + amount);
		
		amount = amount + amount * interestRate / 100;
		System.out.println("After 2 years " + amount);

		amount = amount + amount * interestRate / 100;
		System.out.println("After 3 years " + amount);
	}

}

package v2019.topic01_DataTypes;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class HelloJavaFX extends Application {
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    Label label = new Label("Hello, JavaFX");

    BorderPane root = new BorderPane();
    root.setCenter(label);

    Scene scene = new Scene(root, 150, 100);

    primaryStage.setTitle("Hello, JavaFX");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
package v2019.topic04_ObjectsRevisited.person0;

import java.util.Scanner;

public class PersonProgram {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter ID: ");
		int ID = in.nextInt();
		in.nextLine();
		System.out.println("Enter last name: ");
		String lastname = in.nextLine();
		System.out.println("Enter first name: ");
		String firstname = in.nextLine();
		System.out.println("Enter gender (f = female / all else = male): ");
		String tmpString = in.nextLine();
		int gender = 0;
		if (tmpString.startsWith("f")) gender = 1;
		
		Person p = new Person(ID, lastname, firstname);
		p.setGender(gender);
		
		System.out.println(p.toString());
		
		in.close();
	}
}

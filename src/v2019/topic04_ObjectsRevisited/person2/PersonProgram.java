package v2019.topic04_ObjectsRevisited.person2;

import java.util.Scanner;

public class PersonProgram {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Person p1 = readPerson(in);
		Person p2 = readPerson(in);
		Person p3 = readPerson(in);

		System.out.println(p1.toString());
		System.out.println(p2.toString());
		System.out.println(p3.toString());

		if (p1.getAge().equals(p2.getAge())) {
			printSameAge(p1, p2);
		} else if (p2.getAge().equals(p3.getAge())) {
			printSameAge(p2, p3);
		} else if (p1.getAge().equals(p3.getAge())) {
			printSameAge(p1, p3);
		}
	}

	private static Person readPerson(Scanner in) {
		System.out.println("Enter last name: ");
		String lastname = in.nextLine();
		System.out.println("Enter first name: ");
		String firstname = in.nextLine();
		System.out.println("Enter gender (f = female / all else = male): ");
		String tmpString = in.nextLine();
		Gender gender = Gender.MALE;
		if (tmpString.startsWith("f"))
			gender = Gender.FEMALE;
		System.out.println("Enter the person's age: ");
		Integer age = in.nextInt();
		in.nextLine(); // Skip end of line

		Person p = new Person(lastname, firstname);
		p.setGender(gender);
		p.setAge(age);

		return p;
	}

	private static void printSameAge(Person p, Person q) {
		System.out.println(p.getFirstName() + " " + p.getLastName() + " and " + q.getFirstName() + q.getLastName()
				+ " are the same age!");
	}
}

package v2019.topic04_ObjectsRevisited.person2;

public class Person {
	private static int highestID = -1;
	
	private final int ID;
	private String lastname;
	private String firstname;
	private Gender gender;
	private Integer age;
	
	// Class method to get the next available ID
	  private static int getNextID() {
	    highestID++;
	    return highestID;
	  }
	  
	public Person() {
		this.ID = getNextID();
	}
	
	public Person(String lastname, String firstname) {
		this(); // Call to constructor with no parameters
		this.lastname = lastname;
		this.firstname = firstname;
	}
	
	@Override
	public String toString() {
		return ID + ": " + lastname + ", " + firstname + " (gender = "
				+ gender.toString() + ") is " + age + " years old";
	}

	// --- Getters and Setters ---
	
	public String getLastName() {
		return lastname;
	}

	public void setLastName(String lastname) {
		if (!lastname.isEmpty()) this.lastname = lastname;
	}

	public String getFirstName() {
		return firstname;
	}

	public void setFirstName(String firstname) {
		if (!firstname.isEmpty()) this.firstname = firstname;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public Integer getAge() {
		return age;
	}
	
	public void setAge(Integer age) {
		this.age = age;
	}

	public int getID() {
		return ID;
	}
}
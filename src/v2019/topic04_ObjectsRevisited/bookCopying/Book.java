package v2019.topic04_ObjectsRevisited.bookCopying;

import java.time.LocalDate;

import v2019.topic04_ObjectsRevisited.person2.Person;

public class Book {
	private final String ISBN;
	private Person author;
	private String title;
	private LocalDate printingDate;
	
	public Book(String ISBN, Person author, String title) {
		this.ISBN = ISBN;
		this.author = author;
		this.title = title;
	}
}

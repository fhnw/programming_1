package v2019.topic04_ObjectsRevisited.person1;

import java.util.Scanner;

public class PersonProgram {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter last name: ");
		String lastname = in.nextLine();
		System.out.println("Enter first name: ");
		String firstname = in.nextLine();
		System.out.println("Enter gender (f = female / all else = male): ");
		String tmpString = in.nextLine();
		Gender gender = Gender.MALE;
		if (tmpString.startsWith("f")) gender = Gender.FEMALE;
		
		Person p = new Person(lastname, firstname);
		p.setGender(gender);
		
		System.out.println(p.toString());
	}
}

package v2019.oop.lecture13_lambda;

import java.util.ArrayList;

import v2019.oop.lecture13_lambda.Card.Rank;
import v2019.oop.lecture13_lambda.Card.Suit;

public class CardLambda {

	public static void main(String[] args) {
		ArrayList<Card> cards = new ArrayList<>();
		for (Rank r : Card.Rank.values()) {
			for (Suit s : Card.Suit.values()) {
				cards.add(new Card(r, s));
			}
		}
		
		cards.stream().filter( c -> c.getSuit()== Card.Suit.HEART)
		.filter( c -> c.getRank() == Card.Rank.THREE)
		.forEach(c -> System.out.println(c));
		
		cards.stream().map( c -> c.getRank())
		.filter(r -> r == Card.Rank.THREE)
		.forEach( r -> System.out.println(r));
	}

}

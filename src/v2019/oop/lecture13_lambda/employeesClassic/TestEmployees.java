package v2019.oop.lecture13_lambda.employeesClassic;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestEmployees {

	public static void main(String[] args) {
		Employees employees = new Employees(); // New test object
		createTestData(employees);

		// Get all employees of each gender
		ArrayList<Person> men = employees.selectByGender(Gender.MALE);
		ArrayList<Person> women = employees.selectByGender(Gender.FEMALE);
		System.out.println("There are " + men.size() + " men and " + women.size() + " women");

		// Get list of all employees who are 65 or older
		ArrayList<Person> retirementAge = employees.selectByAge(65);
		System.out.println("There are " + retirementAge.size() + " employees of retirement age");
		
		// Get list of all employees whose last name begins with 'S'
		ArrayList<Person> sEmployees = employees.selectByFirstLetter('S');
		System.out.println("There are " + sEmployees.size() + " employees whose last name begins with 'S'");
		
	}

	private static void createTestData(Employees employees) {
		employees.addEmployee(new Person("Munoz", "Henry", Gender.MALE, LocalDate.of(1967,12, 20)));
		employees.addEmployee(new Person("Lindsey", "Sonja", Gender.FEMALE, LocalDate.of(1950, 1, 29)));
		employees.addEmployee(new Person("Dennis", "Veronica", Gender.MALE, LocalDate.of(1963,10, 15)));
		employees.addEmployee(new Person("Jackson", "Opal", Gender.FEMALE, LocalDate.of(1967, 3, 2)));
		employees.addEmployee(new Person("Strickland", "Celia", Gender.FEMALE, LocalDate.of(1938, 4, 17)));
		employees.addEmployee(new Person("Allen", "Caleb", Gender.MALE, LocalDate.of(1974, 4, 6)));
		employees.addEmployee(new Person("Carter", "Harriet", Gender.FEMALE, LocalDate.of(1992, 4, 3)));
		employees.addEmployee(new Person("Cummings", "Gabriel", Gender.MALE, LocalDate.of(1993,10, 7)));
		employees.addEmployee(new Person("Obrien", "Tommie", Gender.MALE, LocalDate.of(1969, 6, 1)));
		employees.addEmployee(new Person("Chandler", "Timmy", Gender.MALE, LocalDate.of(1948, 4, 28)));
	}

}

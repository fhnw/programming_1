package v2019.oop.lecture13_lambda.employeesClassic;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class Employees {
	private final ArrayList<Person> employees = new ArrayList<>();
	
	public int countEmployees() {
		return employees.size();
	}
	
	public void addEmployee(Person newPerson) {
		employees.add(newPerson);
	}
	
	public boolean deleteEmployee(Person oldPerson) {
		return employees.remove(oldPerson);
	}
	
	public boolean deleteEmployeeByID(int oldID) {
		boolean found = false;
		for (int i = 0; i < employees.size() && !found; i++) {
			Person p = employees.get(i);
			if (p.getID()==oldID) {
				employees.remove(i);
				found = true;
			}
		}
		return found;
	}
	
	public ArrayList<Person> selectByGender(Gender g) {
		ArrayList<Person> selected = new ArrayList<>();
		for (Person p : employees) {
			if (p.getGender() == g) selected.add(p);
		}
		return selected;
	}
	
	public ArrayList<Person> selectByAge(int minAge) {
		ArrayList<Person> selected = new ArrayList<>();
		LocalDate today = LocalDate.now();
		for (Person p : employees) {
			Period difference = Period.between(p.getBirthDate(), today);
			if (difference.getYears() >= minAge) selected.add(p);
		}
		return selected;
	}
	
	public ArrayList<Person> selectByFirstLetter(char inChar) {
		ArrayList<Person> selected = new ArrayList<>();
		for (Person p : employees) {
			String lastName = p.getLastName();
			if (lastName.charAt(0) == inChar) selected.add(p);
		}
		return selected;
	}
}

package v2019.oop.lecture13_lambda.employeesInterfaces;

public interface Filter<Candidate, Value> {
	public boolean filter(Candidate c, Value v);
}

package v2019.oop.lecture13_lambda.employeesInterfaces;

public enum Gender {
	MALE, FEMALE;
}

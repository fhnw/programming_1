package v2019.oop.lecture13_lambda.employeesInterfaces;

import java.time.LocalDate;
import java.time.Period;

public class FilterByAge implements Filter<Person, Integer> {
	@Override
	public boolean filter(Person p, Integer a) {
		LocalDate today = LocalDate.now();
		Period difference = Period.between(p.getBirthDate(), today);
		return (difference.getYears() >= a);
	}
}

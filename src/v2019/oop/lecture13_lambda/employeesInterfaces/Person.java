package v2019.oop.lecture13_lambda.employeesInterfaces;

import java.time.LocalDate;

public class Person {
	private static int highestID = -1;
	
	private final int ID;
	private String lastName;
	private String firstName;
	private Gender gender;
	private LocalDate birthDate;

	// Class method to get the next available ID
	  private static int getNextID() {
	    highestID++;
	    return highestID;
	  }
	  
	private Person() {
		this.ID = getNextID();
	}
	
	public Person(String lastName, String firstName, Gender gender, LocalDate birthDate) {
		this(); // Call to constructor with no parameters
		this.lastName = lastName;
		this.firstName = firstName;
		this.gender = gender;
		this.birthDate = birthDate;
	}
	
	@Override
	public String toString() {
		return lastName + ", " + firstName + " (ID = " + ID + ")";
	}

	// --- Getters and Setters ---
	
	public int getID() {
		return ID;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (!lastName.isEmpty()) this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (!firstName.isEmpty()) this.firstName = firstName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
}

package v2019.oop.lecture13_lambda.employeesInterfaces;

import java.util.ArrayList;

public class Employees {
	private final ArrayList<Person> employees = new ArrayList<>();
	
	public int countEmployees() {
		return employees.size();
	}
	
	public void addEmployee(Person newPerson) {
		employees.add(newPerson);
	}
	
	public boolean deleteEmployee(Person oldPerson) {
		return employees.remove(oldPerson);
	}
	
	public boolean deleteEmployeeByID(int oldID) {
		boolean found = false;
		for (int i = 0; i < employees.size() && !found; i++) {
			Person p = employees.get(i);
			if (p.getID()==oldID) {
				employees.remove(i);
				found = true;
			}
		}
		return found;
	}
	
	public <Value> ArrayList<Person> select(Filter<Person, Value> filter, Value value) {
		ArrayList<Person> selected = new ArrayList<>();
		for (Person p : employees) {
			if (filter.filter(p, value)) selected.add(p);
		}
		return selected;
	}
}

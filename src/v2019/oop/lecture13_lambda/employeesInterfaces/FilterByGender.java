package v2019.oop.lecture13_lambda.employeesInterfaces;

public class FilterByGender implements Filter<Person, Gender> {
	@Override
	public boolean filter(Person p, Gender g) {
		return (p.getGender() == g);
	}
}

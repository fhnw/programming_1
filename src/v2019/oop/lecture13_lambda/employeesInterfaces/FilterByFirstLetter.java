package v2019.oop.lecture13_lambda.employeesInterfaces;

public class FilterByFirstLetter implements Filter<Person, Character> {
	@Override
	public boolean filter(Person p, Character c) {
		String lastName = p.getLastName();
		return (lastName.charAt(0) == c);
	}
}

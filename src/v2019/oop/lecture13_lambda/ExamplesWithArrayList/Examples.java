package v2019.oop.lecture13_lambda.ExamplesWithArrayList;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Examples {

	public static void main(String[] args) {
		ArrayList<Person> people = createTestData();
		
		// A stream of all people in the ArrayList
		Stream<Person> stream = people.stream();
		
		// Stream with only women
		Stream<Person> women = people.stream().filter( p -> p.getGender() == Gender.FEMALE );

		// Stream with only women whose names begin with 'C'
		Stream<Person> women1 = people.stream()
				.filter( p -> p.getGender() == Gender.FEMALE )
				.filter( p -> p.getLastName().charAt(0) == 'C');		
		
		// Print names of all women
		people.stream()
				.filter( p -> p.getGender() == Gender.FEMALE )
				.forEach( p -> System.out.println(p.getFirstName() + p.getLastName()) );
		
		// Map Person-objects to Strings (of the form "LastName, FirstName")
		Stream<String> names = people.stream().map( p -> p.getLastName() + ", " + p.getFirstName() );
		
		// Map Person-objects to Strings (of the form "LastName, FirstName"), and print them
		people.stream()
			.map( p -> p.getLastName() + ", " + p.getFirstName() )
			.forEach( s -> System.out.println(s) );
		
		// Get a new List containing all people over 65
		List<Person> retiredPeople = people.stream().filter( p -> {
			Period difference = Period.between(p.getBirthDate(), LocalDate.now());
			return (difference.getYears() >= 65);
		}).collect(Collectors.toList());
	}
	
	private static ArrayList<Person> createTestData() {
		ArrayList<Person> people = new ArrayList<>();
		people.add(new Person("Munoz", "Henry", Gender.MALE, LocalDate.of(1967,12, 20)));
		people.add(new Person("Lindsey", "Sonja", Gender.FEMALE, LocalDate.of(1950, 1, 29)));
		people.add(new Person("Dennis", "Veronica", Gender.MALE, LocalDate.of(1963,10, 15)));
		people.add(new Person("Jackson", "Opal", Gender.FEMALE, LocalDate.of(1967, 3, 2)));
		people.add(new Person("Strickland", "Celia", Gender.FEMALE, LocalDate.of(1938, 4, 17)));
		people.add(new Person("Allen", "Caleb", Gender.MALE, LocalDate.of(1974, 4, 6)));
		people.add(new Person("Carter", "Harriet", Gender.FEMALE, LocalDate.of(1992, 4, 3)));
		people.add(new Person("Cummings", "Gabriel", Gender.MALE, LocalDate.of(1993,10, 7)));
		people.add(new Person("Obrien", "Tommie", Gender.MALE, LocalDate.of(1969, 6, 1)));
		people.add(new Person("Chandler", "Timmy", Gender.MALE, LocalDate.of(1948, 4, 28)));
		return people;
	}

}

package v2019.oop.lecture13_lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ExamplesWithInteger {
	public static void main(String[] args) {
	  ArrayList<Integer> numbers = createTestData();
	  
	  // Print the ten numbers, so we know what they are
	  numbers.stream().forEach( i -> System.out.print(i + " ") );
	  System.out.println();
	  
	  // Use Filter and ForEach to print all numbers > 50
	  numbers.stream().filter( i -> i > 50 ).forEach( i -> System.out.print(i + " "));
	  System.out.println();
	  
	  // Use Collect to place all numbers > 50 in a new list
	  // Then Use Filter and ForEach on the new list to print all numbers < 75
	  List<Integer> bigNums = numbers.stream().filter( i -> i > 50).collect(Collectors.toList());
	  bigNums.stream().filter( i -> i < 75).forEach( i -> System.out.print(i + " ") );
	  System.out.println();
	  
	  // Use Map to create a new List<Boolean>, the use ForEach to print all values
	  numbers.stream().map( i -> i > 50 ).forEach( b -> System.out.print(b + " "));
	  System.out.println();
	}

	private static ArrayList<Integer> createTestData() {
		ArrayList<Integer> testData = new ArrayList<>();
		for (int i = 0; i < 10; i++) testData.add( (int) ( Math.random() * 100) );
		return testData;
	}

}

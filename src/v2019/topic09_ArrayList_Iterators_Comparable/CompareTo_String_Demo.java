package v2019.topic09_ArrayList_Iterators_Comparable;

public class CompareTo_String_Demo {
	public static void main(String[] args) {
		compareTwoStrings("abc", "xyz");
		compareTwoStrings("abc", "abc");
		compareTwoStrings("abc", "aaa");
	}
	
	private static void compareTwoStrings(String x, String y) {
		System.out.print(x + ".compareTo(" + y + ") gives result ");
		System.out.println(x.compareTo(y));
	}
}

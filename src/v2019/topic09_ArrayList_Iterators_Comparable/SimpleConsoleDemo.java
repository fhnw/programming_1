package v2019.topic09_ArrayList_Iterators_Comparable;

import java.util.ArrayList;
import java.util.Iterator;

public class SimpleConsoleDemo {

	public static void main(String[] args) {
		ArrayList<String> myStrings = new ArrayList<String>();

		myStrings.add("Gryffindor");
		myStrings.add("Ravenclaw");
		myStrings.add("Hufflepuff");
		myStrings.add("Slytherin");

        // Sort strings - only works if they implement Comparable
        myStrings.sort(null);

		int size = myStrings.size(); // Checking size of ArrayList
		System.out.println("ArrayList contains " + size + " elements:");
		
		// Print strings
		for (int i = 0; i < size; i++) {
		    System.out.println(i + ": " + myStrings.get(i));
		}

        // Remove the first occurrence
        myStrings.remove("Ravenclaw");

		// If not empty, print whatever is left
		if (!myStrings.isEmpty()) {
		    System.out.println("\nRemaining strings:");
		    for (String s : myStrings) System.out.println(s);
		}
	}
}

package v2019.topic09_ArrayList_Iterators_Comparable.personDemo;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import v2019.topic08_JavaFX_Intro.personGUI.Person;
import v2019.topic08_JavaFX_Intro.personGUI.Person.Gender;

public class PersonDemo extends Application {
	// Controls used to enter person data
	TextField txtPersonLastName = new TextField();
	TextField txtPersonFirstName = new TextField();
	TextField txtPersonGender = new TextField();

	// Controls used to display person data
	Label lblPersonID = new Label();
	Label lblPersonLastName = new Label();
	Label lblPersonFirstName = new Label();
	Label lblPersonGender = new Label();

	// Buttons
    Button newButton = new Button("New");
    Button leftButton = new Button("\u2190");   // Unicode left-arrow
    Button rightButton = new Button("\u2192");  // Unicode right-arrow
	
	// ArrayList for storing Person-objects
    ArrayList<Person> people = new ArrayList<>();
    
    // Index of person currently displayed
    int personDisplayed = -1;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		VBox root = new VBox();
		root.getChildren().add(createPersonEntryPane());
		root.getChildren().add(createButtonPane());
		root.getChildren().add(createPersonDisplayPane());

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("PersonDemo.css").toExternalForm());
		primaryStage.setTitle("Person Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private Pane createPersonEntryPane() {
		GridPane pane = new GridPane();
		pane.setId("personEntry"); // ID for CSS formatting

		// The static labels
		Label lblLastName = new Label("Last name: ");
		Label lblFirstName = new Label("First name: ");
		Label lblGender = new Label("Gender: ");

		// Add the controls (col, row)
		pane.add(lblLastName, 0, 0);
		pane.add(txtPersonLastName, 1, 0);
		pane.add(lblFirstName, 0, 1);
		pane.add(txtPersonFirstName, 1, 1);
		pane.add(lblGender, 0, 2);
		pane.add(txtPersonGender, 1, 2);

		return pane;
	}

	private Pane createButtonPane() {
		HBox pane = new HBox();
		pane.setAlignment(Pos.CENTER);
		pane.setId("buttonArea"); // ID for CSS formatting

		pane.getChildren().addAll(leftButton, newButton, rightButton);

        leftButton.setOnAction( e -> { moveLeft(); });

		newButton.setOnAction( e -> { saveNewPerson(); });

        rightButton.setOnAction( e -> { moveRight(); });

		return pane;
	}

	private Pane createPersonDisplayPane() {
		GridPane pane = new GridPane();
		pane.setId("personDisplay"); // ID for CSS formatting

		// The static labels
		Label lblID = new Label("Person ID: ");
		Label lblLastName = new Label("Last name: ");
		Label lblFirstName = new Label("First name: ");
		Label lblGender = new Label("Gender: ");

		// Add the controls (col, row)
		pane.add(lblID, 0, 0);
		pane.add(lblPersonID, 1, 0);
		pane.add(lblLastName, 0, 1);
		pane.add(lblPersonLastName, 1, 1);
		pane.add(lblFirstName, 0, 2);
		pane.add(lblPersonFirstName, 1, 2);
		pane.add(lblGender, 0, 3);
		pane.add(lblPersonGender, 1, 3);

		return pane;
	}
	
	private void saveNewPerson() {
        // Create the person-object
		String strGender = txtPersonGender.getText();
		Gender gender = strGender.equals("male") ? Gender.MALE : Gender.FEMALE;
		
        Person person = new Person(txtPersonLastName.getText(), txtPersonFirstName.getText(), gender);
        
        // Add it to the ArrayList
        people.add(person);
        
        // Display the newly added person
        displayPerson(people.size() - 1);
	}
	
	/**
	 * If the ArrayList is not empty, and we are not at the start, move one position towards the beginning of the ArrayList
	 * and display the next person. If this is not possible, do nothing.
	 */
	private void moveLeft() {
	    if (people.size() > 0 && personDisplayed > 0) {
	        displayPerson(personDisplayed-1);
	    }
	}

    /**
     * If the ArrayList is not empty, and we are not at the end, move one position towards the end of the ArrayList
     * and display the next person. If this is not possible, do nothing.
     */
    private void moveRight() {
        if (people.size() > 0 && personDisplayed < (people.size() - 1)) {
            displayPerson(personDisplayed+1);
        }
    }
	
	private void displayPerson(int index) {
	    // Fetch the object from the ArrayList
	    Person p = people.get(index);
	    
        // Display the person data
        lblPersonID.setText(Integer.toString(p.getID()));
        lblPersonLastName.setText(p.getLastName());
        lblPersonFirstName.setText(p.getFirstName());
        lblPersonGender.setText(p.getGender().toString());
	    
        // Save the number of the person displayed, for use
        // by the arrow buttons
        personDisplayed = index;
	}
}

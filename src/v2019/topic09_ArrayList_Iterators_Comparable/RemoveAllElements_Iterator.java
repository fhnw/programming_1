package v2019.topic09_ArrayList_Iterators_Comparable;

import java.util.ArrayList;
import java.util.Iterator;

public class RemoveAllElements_Iterator {

	public static void main(String[] args) {
		ArrayList<String> myStrings = new ArrayList<String>();

		myStrings.add("Gryffindor");
		myStrings.add("Ravenclaw");
		myStrings.add("Hufflepuff");
		myStrings.add("Slytherin");

        // Sort strings - only works if they implement Comparable
        myStrings.sort(null);

		int size = myStrings.size(); // Checking size of ArrayList
		System.out.println("ArrayList contains " + size + " elements:");
		
		// Print and remove all strings
		Iterator<String> iter = myStrings.iterator();
		while (iter.hasNext()) {
			String myString = iter.next();
			System.out.println(myString);
			iter.remove(); // Removes the current element of the ArrayList
		}
	}
}

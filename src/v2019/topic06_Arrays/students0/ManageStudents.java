package v2019.topic06_Arrays.students0;

import java.util.Scanner;

public class ManageStudents {
	private static final int MAX_STUDENTS = 7;
	private static final Person[] students = new Person[MAX_STUDENTS];
	private static int numStudents = 0; // Actual number of students in array

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String command = "";
		while (!command.equals("quit")) {
			System.out.println("Enter command: add, delete, print");
			command = in.nextLine();

			switch (command) {
			case "add":
				addPerson(in);
				break;
			case "delete":
				deletePerson(in);
				break;
			case "print":
				printPeople();
				break;
			case "quit":
				// Do nothing; loop will end
				break;
			default:
				System.out.println("Invalid command!");
			}
		}
		in.close();
	}

	// Methods empty for now
	private static void addPerson(Scanner in) { }
	private static void deletePerson(Scanner in) { }
	private static void printPeople() { }
}

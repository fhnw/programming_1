package v2019.topic06_Arrays.students0;

public class Person {
	private static int highestID = -1;
	
	private int ID;
	private String lastname;
	private String firstname;
	private Gender gender;
	
	// Class method to get the next available ID
	  private static int getNextID() {
	    highestID++;
	    return highestID;
	  }
	  
	public Person() {
		this.ID = getNextID();
	}
	
	public Person(String lastname, String firstname) {
		this(); // Call to constructor with no parameters
		this.lastname = lastname;
		this.firstname = firstname;
	}
	
	@Override
	public String toString() {
		return ID + ": " + lastname + ", " + firstname + " (gender = "
				+ gender.toString() + ")";
	}

	// --- Getters and Setters ---
	
	public String getLastName() {
		return lastname;
	}

	public void setLastName(String lastname) {
		if (!lastname.isEmpty()) this.lastname = lastname;
	}

	public String getFirstName() {
		return firstname;
	}

	public void setFirstName(String firstname) {
		if (!firstname.isEmpty()) this.firstname = firstname;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getID() {
		return ID;
	}
}
package v2019.topic06_Arrays.hackerRank;

public class SimpleQueries {
	/*
	 * Complete the function below.
	 */
	static int[] counts(int[] nums, int[] maxes) {
		int x = maxes.length;
		int[] result = new int[x];
		int y = maxes.length;
		for (int fill = 0; fill < maxes.length; fill++) {
			result[fill] = 0;
		}
		for (int i = 0; i < nums.length; i++) {
			int varnum = nums[i];
			for (int i2 = 0; i2 < y; i2++) {
				if (varnum <= maxes[i2]) result[i2]++;
			}
		}
		return result;
	}
}
package v2019.topic06_Arrays.hackerRank;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class SimpleQueriesTest {

	@Test
	public void test1() {
		int[] nums = {1, 2, 3, 4};
		int[] maxes = {3, 4};
		int[] result = {3, 4};
		
		int[] actualResult = SimpleQueries.counts(nums,  maxes);
		assertTrue(Arrays.equals(result, actualResult));
	}

	/**
	 * If we take all numbers from 1 to 10000, we can use these both as
	 * the nums and as the maxes. Each number is then its own expected
	 * result. For example, the number of values <= 3 is exactly 3.
	 * 
	 * You can vary the size of the test. As the size increases past 10000,
	 * many programs will be come too slow. To work with 100000 elements or
	 * more, you will need to optimize your program.
	 */
	@Test
	public void test2() {
		final int size = 10000;
		int[] nums = new int[size];
		
		for (int i = 1; i <= size; i++) {
			nums[i-1] = i;
		}
		
		int[] actualResult = SimpleQueries.counts(nums,  nums);

		assertTrue(Arrays.equals(nums, actualResult));
	}
}

package v2019.topic06_Arrays.students1;

import java.util.Scanner;

public class ManageStudents {
	private static final int MAX_STUDENTS = 7;
	private static final Person[] students = new Person[MAX_STUDENTS];
	private static int numStudents = 0; // Actual number of students in array

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String command = "";
		while (!command.equals("quit")) {
			System.out.println("Enter command: add, delete, print");
			command = in.nextLine();

			switch (command) {
			case "add":
				addPerson(in);
				break;
			case "delete":
				deletePerson(in);
				break;
			case "print":
				printPeople();
				break;
			case "quit":
				// Do nothing; loop will end
				break;
			default:
				System.out.println("Invalid command!");
			}
		}
		in.close();
	}

	/**
	 * If the array is not full, read data from scanner,
	 * create new Person object and add to array
	 */
	private static void addPerson(Scanner in) {
		if (numStudents >= MAX_STUDENTS) {
			System.out.println("Student array is full!");
		} else {
			System.out.println("Enter last name: ");
			String lastname = in.nextLine();
			System.out.println("Enter first name: ");
			String firstname = in.nextLine();
			System.out.println("Enter gender (f = female / all else = male): ");
			String tmpString = in.nextLine();
			Gender gender = Gender.MALE;
			if (tmpString.startsWith("f")) gender = Gender.FEMALE;
			
			Person p = new Person(lastname, firstname);
			p.setGender(gender);
			
			students[numStudents++] = p;
		}		
	}
	
	/**
	 * Ask the user which person to delete. Validate the user input.
	 * If it is acceptable, delete the person, then shift remaining
	 * entries to eliminate the hole in the array.
	 */
	private static void deletePerson(Scanner in) {
		System.out.print("Which entry do you want to delete (0 - ");
		System.out.print((numStudents-1) + ")?");
		int index = in.nextInt();
		in.nextLine();
		if (index >= 0 && index < numStudents) {
			students[index] = null; // delete student from array
			
			// Shift array to fill hole
			for (int i = index+1; i < numStudents; i++) {
				students[i-1] = students[i];
			}
			students[numStudents-1] = null;
			
			// Reduce number of students by one
			numStudents--;			
		} else {
			System.out.println("Invalid number");
		}
	}
	
	private static void printPeople() {
		for (int i = 0; i < numStudents; i++) {
			System.out.print("(Entry " + i + " in array)  ");
			System.out.println(students[i].toString());
		}
	}
}

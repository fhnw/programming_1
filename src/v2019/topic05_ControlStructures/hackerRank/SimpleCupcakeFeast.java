package v2019.topic05_ControlStructures.hackerRank;

import java.util.Scanner;

public class SimpleCupcakeFeast {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int c = in.nextInt();
		int m = in.nextInt();
		in.close();
		
		int answer = maximumCupcakes(n, c, m);
		System.out.println(answer);
	}

	public static int maximumCupcakes(int n, int c, int m) {
		int totalCupcakes = 0;
		int wrappers = 0;
		int cupcakes = n / c; // initial purchase
		while (cupcakes > 0) {
			wrappers += cupcakes; // eat the cupcakes, save the wrappers
			totalCupcakes += cupcakes; // total cupcake count
			cupcakes = wrappers / m; // get more cupcakes...
			wrappers -= m * cupcakes; // ...in return for our wrappers
		}
		return totalCupcakes;
	}	
}

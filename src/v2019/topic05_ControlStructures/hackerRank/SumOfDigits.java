package v2019.topic05_ControlStructures.hackerRank;

import java.util.Scanner;

public class SumOfDigits {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int value = in.nextInt();
		in.close();
		
		int answer = sumOfdigits(value);
		System.out.println(answer);
	}
	
	static int sumOfdigits(int valueIn) {
		int sum = 0;
		while (valueIn > 0) {
			sum += valueIn % 10;
			valueIn = valueIn / 10;
		}
		return sum;
    }
}

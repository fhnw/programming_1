package v2019.topic05_ControlStructures;

public class FizzBuzz {

	public static void main(String[] args) {
		for (int i = 1; i <= 100; i++) {
			String s = "";
			if (i % 3 == 0) s = "Fizz";
			if (i % 5 == 0) s = s + "Buzz";
			if (s.isEmpty()) s = Integer.toString(i);
			System.out.print(s + " ");
		}
	}
}

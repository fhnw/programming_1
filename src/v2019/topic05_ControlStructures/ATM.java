package v2019.topic05_ControlStructures;

import java.util.Scanner;

public class ATM {

	public static void main(String[] args) {
		final int CORRECT_PIN = 1234;
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter PIN");
		int PIN = userInput.nextInt();

		int numTries = 1;
		while (PIN != CORRECT_PIN && numTries < 3) {
			System.out.println("Incorrect PIN - try again");
			PIN = userInput.nextInt();
		}
		
		if (PIN == CORRECT_PIN) {
			System.out.println("Enter amount");
			int amount = userInput.nextInt();
			if (amount > 0 && amount % 10  == 0) {
				System.out.println("Here is your money");	
			} else {
				System.out.println("Bad amount");
			}
		} else {
			System.out.println("Wrong PIN 3 times - your card is now blocked");
		}
		userInput.close();
	}
}

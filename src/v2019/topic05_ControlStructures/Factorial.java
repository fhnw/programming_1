package v2019.topic05_ControlStructures;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter an integer, or 0 to quit");
		int value = in.nextInt();
		while (value > 0) {
			int result = factorial(value);
			int result2 = factorialWithFor(value);
			System.out.println("Two ways to calculate the factorial of " + value + ": " + result + " and " + result2);

			System.out.println("Enter an integer, or 0 to quit");
			value = in.nextInt();
		}
		in.close(); // It's good manners to close your scanner when done
	}

	public static int factorial(int valIn) {
		int answer = 1;
		while (valIn > 1) {
			answer = answer * valIn;
			valIn = valIn - 1;
		}
		return answer;
	}
	
	public static int factorialWithFor(int valIn) {
		int answer = 1;
		for (int mult = 2; mult <= valIn; mult++) {
			answer = answer * mult;
		}
		return answer;
	}	
}

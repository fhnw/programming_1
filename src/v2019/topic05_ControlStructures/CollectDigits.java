package v2019.topic05_ControlStructures;

import java.util.Scanner;

public class CollectDigits {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a string containing some digits");
		String strIn = in.nextLine();
		in.close(); // It's good manners to close your scanner when done

		String digits = collectDigits(strIn);
		System.out.println("I found the following digits: " + digits);
	}

	public static String collectDigits(String valIn) {
		String digits = new String();

		for (int i = 0; i < valIn.length(); i++) {
			char c = valIn.charAt(i);
			if (Character.isDigit(c)) {
				digits = digits + c;
			}
		}

		return digits;
	}
}

package v2019.topic05_ControlStructures;

import java.util.Scanner;

public class Collatz {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter an integer to test, or 0 to quit");
		int value = in.nextInt();
		while (value > 0) {
			int result = collatzSteps(value);
			System.out.println("Collatz steps for " + value + " are " + result);
			
			System.out.println("Enter an integer, or 0 to quit");
			value = in.nextInt();			
		}
		in.close(); // It's good manners to close your scanner when done
	}

	public static int collatzSteps(int valIn) {
		int steps = 0;
		while (valIn > 1) {
			if (valIn % 2 == 1) {
				valIn = valIn * 3 + 1;
			} else {
				valIn = valIn / 2;
			}
			steps++;
		}
		return steps;
	}
}

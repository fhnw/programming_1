package v2019.topic05_ControlStructures;

import java.util.Scanner;

public class Palindrome1 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("How many strings do you want to test?");
		int numTests = s.nextInt();
		s.nextLine(); // Skip end-of-line after the integer
		for (int i = 0; i < numTests; i++) {
			System.out.println("Enter the next string");
			String test = s.nextLine();
			if (isPalindrome(test)) {
				System.out.println("Is a palindrome!");
			} else {
				System.out.println("Is *not* a palindrome!");
			}
		}
		s.close();
	}

	private static boolean isPalindrome(String text) {
		int indexStart = 0;
		int indexEnd = text.length() - 1;
		boolean answer = true;
		while (indexStart < indexEnd) {
			char c = text.charAt(indexStart++);
			char d = text.charAt(indexEnd--);
			answer = answer && (c == d);
		}
		return answer;
	}
}

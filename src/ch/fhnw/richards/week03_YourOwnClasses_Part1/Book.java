package ch.fhnw.richards.week03_YourOwnClasses_Part1;

/**
 * An implementation of the Book class for a book shop
 */
public class Book {
	private String ISBN;
	private String title;
	private String author;
	private int price;
	private int inStock;
	
	public Book(String ISBN, String title, String author) {
		this.ISBN = ISBN;
		this.title = title;
		this.author = author;
		price = 0;
		inStock = 0;
	}
	
	public void sellBooks(int numSold) {
		inStock -= numSold;
	}
	
	public void receiveBooks(int numReceived) {
		inStock += numReceived;
	}
	
	// Getters and setters

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getISBN() {
		return ISBN;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public int getInStock() {
		return inStock;
	}
}

package ch.fhnw.richards.week03_YourOwnClasses_Part1.dice;

import java.util.Random;

public class Dice {
	private int numberOfSides;
	
	public Dice(int n) {
		numberOfSides = n;
	}
	
	// A different constructor
	// for the most common case
	public Dice() {
		numberOfSides = 6;
	}
	
	public int getNumberOfSides() {
		return numberOfSides;
	}
	
	public void setNumberOfSides(int numberOfSides) {
		this.numberOfSides = numberOfSides;
	}
	
	public double theoreticalAverage() {
		double average = (1.0 + numberOfSides) / 2.0;
		return average;
	}
	
	public int rollDice() {
		Random random = new Random();
		int value = random.nextInt(numberOfSides) + 1;
		return value;
	}
}

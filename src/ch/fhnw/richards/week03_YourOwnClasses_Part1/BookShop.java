package ch.fhnw.richards.week03_YourOwnClasses_Part1;

public class BookShop {

	public static void main(String[] args) {
		Book book1 = new Book("978-3658303129", "Java in 14 Wochen", "Kaspar Riesen");
		book1.receiveBooks(5);
		Book book2 = new Book("978-3864908149", "Basiswissen Requirements Engineering", "Klaus Pohl, Chris Rupp");
		book2.receiveBooks(3);
		
		// Sell some books
		book1.sellBooks(2);
		book2.sellBooks(1);
		
		System.out.println("We have " + book1.getInStock() + " of \"" + book1.getTitle() + "\"");
		System.out.println("We have " + book2.getInStock() + " of \"" + book2.getTitle() + "\"");
	}
}

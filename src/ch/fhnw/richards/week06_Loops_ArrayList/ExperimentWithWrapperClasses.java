package ch.fhnw.richards.week06_Loops_ArrayList;

public class ExperimentWithWrapperClasses {

	public static void main(String[] args) {
		Integer x=13;
		printInteger(x);

	}

	/**
	 * Method that prints an Integer
	 */
	private static void printInteger(Integer in) {
		System.out.println(in);
		System.out.println(Integer.toString(in));
		System.out.println(in.toString());
	}
}

package ch.fhnw.richards.week11_Multiclass.animalClinic;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class PetEntryPane extends GridPane {
	// Controls used to enter pet data
	TextField txtName = new TextField();
	ComboBox<Gender> cmbGender = new ComboBox<>();
	ComboBox<Species> cmbSpecies = new ComboBox<>();

	public PetEntryPane() {
		super(); // Initialize our superclass: GridPane
		this.setId("petEntry"); // ID for CSS formatting

		// The static labels
		Label lblName = new Label("Name: ");
		Label lblGender = new Label("Gender: ");
		Label lblSpecies = new Label("Species: ");
		
		// Fill the ComboBox
		cmbGender.getItems().setAll(Gender.values());
		cmbSpecies.getItems().setAll(Species.values());

		// Add the controls (col, row)
		this.addRow(0, lblName, txtName);
		this.addRow(1, lblGender, cmbGender);
		this.addRow(2, lblSpecies, cmbSpecies);
	}
	
	public Pet createPet() {
		String name = txtName.getText();
		Gender gender = cmbGender.getValue();
		Species species = cmbSpecies.getValue();
		return new Pet(name, gender, species);
	}
}

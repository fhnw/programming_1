package ch.fhnw.richards.week11_Multiclass.animalClinic;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class PetDisplayPane extends GridPane {
	// Controls used to display pet data
	Label lblPetID = new Label();
	Label lblPetName = new Label();
	Label lblPetGender = new Label();
	Label lblPetSpecies = new Label();
	Label lblPetOwner = new Label();

	public PetDisplayPane() {
		super(); // Initialize our superclass: GridPane
		this.setId("petDisplay"); // ID for CSS formatting

		// The static labels
		Label lblID_label = new Label("Pet ID: ");
		Label lblName_label = new Label("Name: ");
		Label lblGender_label = new Label("Gender: ");
		Label lblSpecies_label = new Label("Species: ");
		Label lblOwner_label = new Label("Owner: ");

		// Add the controls (col, row)
		this.addRow(0, lblID_label, lblPetID);
		this.addRow(1, lblName_label, lblPetName);
		this.addRow(2, lblGender_label, lblPetGender);
		this.addRow(3, lblSpecies_label, lblPetSpecies);
		this.addRow(4, lblOwner_label, lblPetOwner);
	}

	public void displayPet(Pet pet) {
		if (pet != null) {
			lblPetID.setText(Integer.toString(pet.getID()));
			lblPetName.setText(pet.getName());
			lblPetGender.setText(pet.getGender().toString());
			lblPetSpecies.setText(pet.getSpecies().toString());
			lblPetOwner.setText(pet.getOwner().toString());
		} else {
			lblPetID.setText("");
			lblPetName.setText("");
			lblPetGender.setText("");
			lblPetSpecies.setText("");
			lblPetOwner.setText("");
		}
	}
}

package ch.fhnw.richards.week11_Multiclass.animalClinic;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class ButtonPane extends HBox {
	// Our buttons need a reference back to the main program
	AnimalClinic animalClinic;
	
	// Buttons to perform actions
	Button btnCreate = new Button("Create");
	Button btnUpdate = new Button("Update");
	Button btnDelete = new Button("Delete");
	Button btnLeft = new Button("\u2B60");
	Button btnRight = new Button("\u2B62");

	public ButtonPane(AnimalClinic mainProgram) {
		super(); // Allow superclass HBox to initialize
		
		// Save reference back to main program, because
		// that is where the event-handling methods are
		this.animalClinic = mainProgram;
		
		this.setId("buttonArea"); // ID for CSS formatting
		this.getChildren().addAll(btnLeft, btnCreate, btnUpdate, btnDelete, btnRight);
		
		// Define the actions to take
		btnLeft.setOnAction(animalClinic::btnLeft);
		btnCreate.setOnAction(animalClinic::btnCreate);
		btnUpdate.setOnAction(animalClinic::btnUpdate);
		btnDelete.setOnAction(animalClinic::btnDelete);
		btnRight.setOnAction(animalClinic::btnRight);
	}
}

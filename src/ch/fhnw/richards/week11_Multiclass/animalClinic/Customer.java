package ch.fhnw.richards.week11_Multiclass.animalClinic;

public class Customer {
	private static int nextID = 0;
	
	private final int ID;
	private String lastName;
	private String firstName;
	private Gender gender;
	private Pet pet;
	  
	public Customer(String lastName, String firstName, Gender gender) {
		this.ID = nextID++;
		this.lastName = lastName;
		this.firstName = firstName;
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return lastName + ", " + firstName + " (ID = " + ID + ")";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Customer c = (Customer) o;
		return c.ID == this.ID;
	}

	// --- Getters and Setters ---
	
	public int getID() {
		return ID;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (!lastName.isEmpty()) this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (!firstName.isEmpty()) this.firstName = firstName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public Pet getPet() {
		return pet;
	}
	
	public void setPet(Pet pet) {
		this.pet = pet;
	}
}

package ch.fhnw.richards.week11_Multiclass.animalClinic;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class CustomerEntryPane extends GridPane {
	// Controls used to enter customer data
	TextField txtLastName = new TextField();
	TextField txtFirstName = new TextField();
	ComboBox<Gender> cmbGender = new ComboBox<>();

	public CustomerEntryPane() {
		super(); // Initialize our superclass: GridPane
		this.setId("customerEntry"); // ID for CSS formatting

		// The static labels
		Label lblLastName = new Label("Last name: ");
		Label lblFirstName = new Label("First name: ");
		Label lblGender = new Label("Gender: ");
		
		// Fill the ComboBox
		cmbGender.getItems().setAll(Gender.values());

		// Add the controls (col, row)
		this.addRow(0, lblLastName, txtLastName);
		this.addRow(1, lblFirstName, txtFirstName);
		this.addRow(2, lblGender, cmbGender);
	}
	
	public Customer createCustomer() {
		String lastName = txtLastName.getText();
		String firstName = txtFirstName.getText();
		Gender gender = cmbGender.getValue();
		return new Customer(lastName, firstName, gender);		
	}
}

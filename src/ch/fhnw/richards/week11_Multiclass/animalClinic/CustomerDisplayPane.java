package ch.fhnw.richards.week11_Multiclass.animalClinic;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class CustomerDisplayPane extends GridPane {
	// Controls used to display customer data
	Label lblID = new Label();
	Label lblLastName = new Label();
	Label lblFirstName = new Label();
	Label lblGender = new Label();
	Label lblPet = new Label();

	public CustomerDisplayPane() {
		super(); // Initialize our superclass: GridPane
		this.setId("customerDisplay"); // ID for CSS formatting

		// The static labels
		Label lblID_label = new Label("ID: ");
		Label lblLastName_label = new Label("Last name: ");
		Label lblFirstName_label = new Label("First name: ");
		Label lblGender_label = new Label("Gender: ");
		Label lblPet_label = new Label("Pet: ");

		// Add the controls (col, row)
		this.addRow(0, lblID_label, lblID);
		this.addRow(1, lblLastName_label, lblLastName);
		this.addRow(2, lblFirstName_label, lblFirstName);
		this.addRow(3, lblGender_label, lblGender);
		this.addRow(4, lblPet_label, lblPet);
	}

	public void displayCustomer(Customer customer) {
		if (customer != null) {
			lblID.setText(Integer.toString(customer.getID()));
			lblLastName.setText(customer.getLastName());
			lblFirstName.setText(customer.getFirstName());
			lblGender.setText(customer.getGender().toString());
			lblPet.setText(customer.getPet().toString());
		} else {
			lblID.setText("");
			lblLastName.setText("");
			lblFirstName.setText("");
			lblGender.setText("");
			lblPet.setText("");
		}
	}
}

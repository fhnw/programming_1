package ch.fhnw.richards.week11_Multiclass.animalClinic;

public class Pet {
	private static int nextID = 0;
	
	private final int ID;
	private String name;
	private Gender gender;
	private Species species;
	private Customer owner;
	
	public Pet(String name, Gender gender, Species species) {
		this.ID = nextID++;
		this.name = name;
		this.gender = gender;
		this.species = species;
	}
	
	public String toString() {
		return name + " (ID = " + ID + ")";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Pet p = (Pet) o;
		return p.ID == this.ID;
	}

	// --- Getters and Setters ---
	
	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}

	public Customer getOwner() {
		return owner;
	}

	public void setOwner(Customer owner) {
		this.owner = owner;
	}
}

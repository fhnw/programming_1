package ch.fhnw.richards.week11_Multiclass.animalClinic;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AnimalClinic extends Application {
	// Data stored here
	ArrayList<Customer> customers = new ArrayList<>();
	int currentCustomer = -1; // current customer, or -1 if list is empty

	// Pieces of the GUI
	CustomerEntryPane customerEntry = new CustomerEntryPane();
	PetEntryPane petEntry = new PetEntryPane();
	ButtonPane buttons = new ButtonPane(this);
	CustomerDisplayPane customerDisplay = new CustomerDisplayPane();
	PetDisplayPane petDisplay = new PetDisplayPane();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		VBox root = new VBox();
		root.getChildren().add(customerEntry);
		root.getChildren().add(petEntry);
		root.getChildren().add(buttons);
		root.getChildren().add(customerDisplay);
		root.getChildren().add(petDisplay);

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("AnimalClinic.css").toExternalForm());
		primaryStage.setTitle("AnimalClinic");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * When something has changed, this updates the display panes
	 */
	private void updateDisplay() {
		if (currentCustomer >= 0) {
			Customer customer = customers.get(currentCustomer);
			customerDisplay.displayCustomer(customer);
			petDisplay.displayPet(customer.getPet());
		} else {
			customerDisplay.displayCustomer(null);
			petDisplay.displayPet(null);
		}
	}

	// ----- Methods for handling user actions -----

	public void btnLeft(ActionEvent e) {
		if (currentCustomer > 0) {
			currentCustomer--;
			updateDisplay();
		}
	}

	public void btnRight(ActionEvent e) {
		if (currentCustomer < (customers.size() - 1)) {
			currentCustomer++;
			updateDisplay();
		}
	}

	public void btnCreate(ActionEvent e) {
		Customer customer = customerEntry.createCustomer();
		Pet pet = petEntry.createPet();

		// Hook the objects together
		customer.setPet(pet);
		pet.setOwner(customer);

		// Add to our list
		customers.add(customer);
		currentCustomer = customers.size() - 1;

		// Update the display (sets values of all those labels...)
		updateDisplay();
	}

	public void btnUpdate(ActionEvent e) {
		// Note: we won't actually save the objects, but we will copy their data into
		// the currently selected objects
		Customer customer = customerEntry.createCustomer();
		Pet pet = petEntry.createPet();

		Customer actualCustomer = customers.get(currentCustomer);
		actualCustomer.setFirstName(customer.getFirstName());
		actualCustomer.setLastName(customer.getLastName());
		actualCustomer.setGender(customer.getGender());

		Pet actualPet = actualCustomer.getPet();
		actualPet.setName(pet.getName());
		actualPet.setGender(pet.getGender());
		actualPet.setSpecies(pet.getSpecies());

		updateDisplay();
	}

	public void btnDelete(ActionEvent e) {
		customers.remove(currentCustomer);
		currentCustomer--; // Display the previous customer (negative if none left)
		updateDisplay();
	}
}

There is a short slide-set for the example.

In this package, there is a subpackage with a complete solution,
and another subpackage with just a start. The "start" contains
the main method with the user-interface, plus skeletons for the
three data classes.
package ch.fhnw.richards.week11_Multiclass.inClassExample.solution;

import java.util.ArrayList;

public class Client {
	private static int nextID = 0;
	private final int ID = nextID++;
	private String name;
	private String email;
	private final ArrayList<Book> books = new ArrayList<>();
	
	public Client() {}
	
	public Client(String name, String email) {
		this.name = name;
		this.email = email;
	}
	
	public void addBook(Book book) {
		books.add(book);
	}
	
	/**
	 * Return the book from the client's list
	 */
	public Book removeBook(String ISBN) {
		Book book = null;
		for (int i = 0; book == null && i < books.size(); i++) {
			Book b = books.get(i);
			if (b.getISBN().equals(ISBN)) {
				books.remove(i);
				book = b;
			}
		}		
		return book;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Client c = (Client) o;
		return c.ID == this.ID;
	}
	
	// Getters and Setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getID() {
		return ID;
	}
}

package ch.fhnw.richards.week11_Multiclass.inClassExample.solution;

import java.util.Scanner;

public class Main {
	private static final Library library = new Library();

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int command = -1;
		while (command != 0) {
			System.out.println("1 - Buy book, 2 - Add client, 3 - Checkout, 4 - Checkin, 0 - Quit");
			command = in.nextInt(); in.nextLine();
			
			switch (command) {
			case 1 -> buyBook(in);
			case 2 -> addClient(in);
			case 3 -> checkout(in);
			case 4 -> checkin(in);
			}
		}
		in.close();
	}
	
	private static void buyBook(Scanner in) {
		System.out.println("On separate lines, enter ISBN, author, title, publisher");
		String ISBN = in.nextLine();
		String author = in.nextLine();
		String title = in.nextLine();
		String publisher = in.nextLine();
		Book book = new Book(ISBN, author, title, publisher);
		library.buyBook(book);
	}
	
	private static void addClient(Scanner in) {
		System.out.println("On separate lines, enter name, email");
		String name = in.nextLine();
		String email = in.nextLine();
		Client client = new Client(name, email);
		library.addClient(client);
	}

	private static void checkout(Scanner in) {
		System.out.println("Enter the client ID and the ISBN");
		int ID = in.nextInt(); String ISBN = in.next(); in.nextLine();
		Client client = library.getClient(ID);
		if (client == null) {
			System.out.println("No such client");
		} else if (library.checkout(ISBN, client)) {
			System.out.println("Checkout successful");
		} else {
			System.out.println("Checkout failed");
		}		
	}
	
	private static void checkin(Scanner in) {
		System.out.println("Enter the client ID and the ISBN");
		int ID = in.nextInt(); String ISBN = in.next(); in.nextLine();
		Client client = library.getClient(ID);
		if (client == null) {
			System.out.println("No such client");
		} else if (library.checkin(ISBN, client)) {
			System.out.println("Check in successful");
		} else {
			System.out.println("Check in failed");
		}		
	}
}

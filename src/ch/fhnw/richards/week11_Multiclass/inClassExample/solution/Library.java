package ch.fhnw.richards.week11_Multiclass.inClassExample.solution;

import java.util.ArrayList;

public class Library {
	private final ArrayList<Client> clientList = new ArrayList<>();
	private final ArrayList<Book> bookList = new ArrayList<>();
	
	public Library() {} // Default constructor - would be written for us
	
	public void buyBook(Book book) {
		bookList.add(book);
	}
	
	public void addClient(Client client) {
		clientList.add(client);
	}
	
	/**
	 * Find a client by their ID
	 */
	public Client getClient(int ID) {
		Client client = null;
		for (int i = 0; client == null && i < clientList.size(); i++) {
			Client c = clientList.get(i);
			if (c.getID() == ID) client = c;
		}
		return client;
	}
	
	/**
	 * To checkout a book, a copy must be available in the book list
	 */
	public boolean checkout(String ISBN, Client client) {
		// Find book in the book list
		Book book = null;
		for (int i = 0; book == null && i < bookList.size(); i++) {
			Book b = bookList.get(i);
			if (b.getISBN().equals(ISBN)) {
				bookList.remove(i);
				book = b;
			}
		}		
		
		if (book != null) {
			book.setClient(client);
			client.addBook(book);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * To check in a book, the book must be in the client's list
	 */
	public boolean checkin(String ISBN, Client client) {
		Book book = client.removeBook(ISBN);
		
		if (book != null) {
			book.setClient(null);
			bookList.add(book);
			return true;
		} else {
			return false;
		}
	}

}

package ch.fhnw.richards.week11_Multiclass.inClassExample.solution;

public class Book {
	private final String ISBN;
	private final String author;
	private final String title;
	private final String publisher;
	private Client client = null;

	public Book(String ISBN, String author, String title, String publisher) {
		this.ISBN = ISBN;
		this.author = author;
		this.title = title;
		this.publisher = publisher;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Book b = (Book) o;
		return this.ISBN.equals(b.ISBN);
	}
	
	// Getters and setters. The only setter is for "client"
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getISBN() {
		return ISBN;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public String getPublisher() {
		return publisher;
	}
}

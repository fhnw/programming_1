package ch.fhnw.richards.week11_Multiclass.inClassExample;

import java.util.ArrayList;

public class Library {
	private final ArrayList<Client> clientList = new ArrayList<>();
	private final ArrayList<Book> bookList = new ArrayList<>();
	
	public Library() {} // Default constructor - would be written for us
	
	public void buyBook(Book book) {
		// TODO
	}
	
	public void addClient(Client client) {
		// TODO
	}
	
	/**
	 * Find a client by their ID
	 */
	public Client getClient(int ID) {
		// TODO: look through client list for a matching ID
	}
	
	/**
	 * To checkout a book, a copy must be available in the book list
	 */
	public boolean checkout(String ISBN, Client client) {
		// TODO: look through book list for matching ISBN, remove book from library
		//       if book was found, set client in book, add book to client
	}
	
	/**
	 * To check in a book, the book must be in the client's list
	 */
	public boolean checkin(String ISBN, Client client) {
		// TODO: Use client.removeBook to get book
		//       If successful, remove client from book,
		//       and put book back into library
	}

}

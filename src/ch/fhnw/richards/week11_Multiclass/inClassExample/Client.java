package ch.fhnw.richards.week11_Multiclass.inClassExample;

import java.util.ArrayList;

public class Client {
	private static int nextID = 0;
	private final int ID = nextID++;
	private String name;
	private String email;
	private final ArrayList<Book> books = new ArrayList<>();
	
	public Client() {}
	
	public Client(String name, String email) {
		this.name = name;
		this.email = email;
	}
	
	public void addBook(Book book) {
		// TODO: add book to list this client has checked out
	}
	
	/**
	 * Return the book from the client's list
	 */
	public Book removeBook(String ISBN) {
		// TODO: look through books, find match
		// return matching book or null if not found
	}
	
	@Override
	public boolean equals(Object o) {
		TODO: implement equals using ID
	}
	
	// Getters and Setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getID() {
		return ID;
	}
}

package ch.fhnw.richards.week11_Multiclass.swissCurrency;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class CurrencyInfo extends Application {
	ImageView viewer = new ImageView();
	Label lblValue = new Label();
	Label lblSize = new Label();
	ComboBox<Currency> cmbSelect = new ComboBox<>();
	
	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		cmbSelect.setItems( FXCollections.observableArrayList( Currency.values()));
		cmbSelect.setValue(Currency.Rappen05);
		display(Currency.Rappen05);
		
		HBox selectorBox = new HBox(cmbSelect);
		selectorBox.getStyleClass().add("hbox");
		
		HBox infoBox = new HBox(lblValue, lblSize);
		infoBox.getStyleClass().add("hbox");

		BorderPane root = new BorderPane();
		root.setTop(selectorBox);
		root.setCenter(viewer);
		root.setBottom(infoBox);
		
		cmbSelect.setOnAction(e -> display(cmbSelect.getValue()));
		
		// Create the scene and attach the stylesheet
		Scene scene = new Scene(root, 1024, 800);
		scene.getStylesheets().add(getClass().getResource("currency.css").toExternalForm());
		
		// Set the title and icon for the window
		primaryStage.setTitle("Swiss Currency"); // Title of the window
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("Fr10.png")));
		
		// Set the scene and show the stage
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void display(Currency selection) {
		viewer.setImage(selection.getPicture());
		lblSize.setText(selection.getSize());
		
		int francs = selection.getValueRappen() / 100;
		int rappen = selection.getValueRappen() - (100 * francs);
		String leadingZeroes = (rappen < 10) ? "0" : "";
		lblValue.setText(Currency.Symbol + " " + francs + "." + leadingZeroes + rappen);
	}
}

package ch.fhnw.richards.week11_Multiclass.swissCurrency;

import javafx.scene.image.Image;

public enum Currency {
	Rappen05("Rp5.png", Type.COIN, 5, "17.15mm"),
	Rappen10("Rp10.png", Type.COIN, 10, "19.15mm"),
	Rappen20("Rp20.png", Type.COIN, 20, "21.05mm"),
	Rappen50("Rp50.png", Type.COIN, 50, "18.20mm"),
	Rappen100("Rp100.png", Type.COIN, 100, "23.20mm"),
	Rappen200("Rp200.png", Type.COIN, 200, "27.40mm"),
	Rappen500("Rp500.png", Type.COIN, 500, "31.45mm"),
	Franc10("Fr10.png", Type.BILL, 1000, "70x123mm"),
	Franc20("Fr20.png", Type.BILL, 2000, "70x130mm"),
	Franc50("Fr50.png", Type.BILL, 5000, "70x137mm"),
	Franc100("Fr100.png", Type.BILL, 10000, "70x144"),
	Franc200("Fr200.png", Type.BILL, 20000, "70x151mm"),
	Franc1000("Fr1000.png", Type.BILL, 100000, "70x158mm");
	
	public static final String Symbol = "CHF";
	
	public enum Type { BILL, COIN };

	private final Image picture;
	private final Type type;
	private final int valueRappen;
	private final String size;
	
	private Currency(String picName, Type type, int valueRappen, String size) {
		this.picture = new Image(getClass().getResourceAsStream(picName));
		this.type = type;
		this.valueRappen = valueRappen;
		this.size = size;
	}

	public Image getPicture() {
		return picture;
	}

	public int getValueRappen() {
		return valueRappen;
	}
	
	public String getSize() {
		return size;
	}
}

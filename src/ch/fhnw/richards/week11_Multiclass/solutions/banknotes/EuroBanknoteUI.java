package ch.fhnw.richards.week11_Multiclass.solutions.banknotes;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class EuroBanknoteUI extends Application {

	// UI Components
	private Label lblSerialNumber = new Label("Enter Number");
	private TextField txtSerialNumber = new TextField(); // TextField for serial number
	private Label lblResult = new Label(); // Label for Result
	private Button btnCheck = new Button("Check");
	private Button btnClear = new Button("Clear");
	private Button btnCount = new Button("Count");

	// Number of valid serial numbers we have seen
	private static int counter = 0;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		HBox topBox = new HBox(lblSerialNumber, txtSerialNumber);
		topBox.getStyleClass().add("hbox");

		HBox middleBox = new HBox(btnCheck, btnClear, btnCount);
		middleBox.getStyleClass().add("hbox");

		HBox bottomBox = new HBox(lblResult);
		bottomBox.getStyleClass().add("hbox");

		VBox root = new VBox(topBox, middleBox, bottomBox);
		root.setId("root");

		Scene scene = new Scene(root, 450, 150);
		scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("Euro Banknotes");
		primaryStage.show();

		// Define Action Handlers
		btnClear.setOnAction(this::clear);
		btnCheck.setOnAction(this::check);
		btnCount.setOnAction(this::count);
	}
	
	private void clear(ActionEvent e) {
		txtSerialNumber.setText("");
		lblResult.setText("");
	}

	private void check(ActionEvent e) {
		String serialNumber = txtSerialNumber.getText().toUpperCase();
		if (EuroBanknoteLogic.isValid(serialNumber)) {
			printResultValid();
			counter++; // Increment counter
		} else {
			printResultInvalid();
		}
	}
	
	private void count(ActionEvent e) {
		printCount();
	}
		
	private void printResultValid() {
		lblResult.setText("Serial number is VALID");
		lblResult.setTextFill(Color.GREEN);
	}

	private void printResultInvalid() {
		lblResult.setText("Serial number is INVALID");
		lblResult.setTextFill(Color.RED);
	}

	private void printCount() {
		lblResult.setText("Number of VALID numbers: " + counter);
		lblResult.setTextFill(Color.BROWN);
	}

}

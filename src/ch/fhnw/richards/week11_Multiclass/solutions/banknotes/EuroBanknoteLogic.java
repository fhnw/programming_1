package ch.fhnw.richards.week11_Multiclass.solutions.banknotes;

public class EuroBanknoteLogic {
	public static boolean isValid(String serialNumber) {
		return checkStructure(serialNumber) && checkDigit(serialNumber);	
	}
	
	private static boolean checkStructure(String serialNumber) {
		boolean valid = true; // Let's be optimistic
		if (serialNumber.length() != 12) {
			valid = false;
		} else {
			char firstChar = serialNumber.charAt(0);
			if (!('A' <= firstChar && firstChar <= 'Z')) {
				valid = false;
			}
		}

		for (int i = 1; i < serialNumber.length(); i++) {
			char currentChar = serialNumber.charAt(i);

			if (!('0' <= currentChar && currentChar <= '9')) {
				valid = false;
			}
		}
		return valid;
	}

	private static boolean checkDigit(String serialNumber) {
		int countryCode = serialNumber.charAt(0) - 'A' + 1;
		serialNumber = Integer.toString(countryCode) + serialNumber.substring(1);
		
		int checkSum = 0;
		for (int i = 0; i < serialNumber.length() - 1; i++) {
			char currentChar = serialNumber.charAt(i);
			checkSum += currentChar - '0';
		}

		int remainder = checkSum % 9;
		int checkDigit = (8 - remainder) == 0 ? 9 : (8 - remainder);

		char actualDigitAsChar = serialNumber.charAt(serialNumber.length() - 1);
		int actualDigit = actualDigitAsChar - '0';
		return (checkDigit == actualDigit);
	}
}

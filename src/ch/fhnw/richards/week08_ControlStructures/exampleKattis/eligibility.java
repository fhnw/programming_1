package ch.fhnw.richards.week08_ControlStructures.exampleKattis;

import java.util.Scanner;

public class eligibility {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numStudents = in.nextInt(); in.nextLine();

		for (int i = 0; i < numStudents; i++) {
			readStudent(in);
		}
		in.close();
	}

	private static void readStudent(Scanner in) {
		String name = in.next();
		int studyYear = getYear(in.next());
		int birthYear = getYear(in.next());
		int numCourses = in.nextInt();
		
		System.out.print(name + " ");
		if (studyYear >= 2010 || birthYear >= 1991)
			System.out.println("eligible");
		else if (numCourses > 40)
			System.out.println("ineligible");
		else
			System.out.println("coach petitions");
	}
	
	private static int getYear(String date) {
		return Integer.parseInt(date.substring(0, 4));
	}
}

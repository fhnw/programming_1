package ch.fhnw.richards.week08_ControlStructures.exampleKattis;

import java.util.Scanner;

public class noduplicates {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String sentence = in.nextLine();
		in.close();
		
		String[] words = sentence.split(" ");

		boolean duplicate = false;
		for (int i = 0; i < words.length; i++) {
			for (int j = i+1; j < words.length; j++) {
				if (words[i].equals(words[j])) duplicate = true;
			}
		}

		System.out.println( duplicate ? "yes" : "no" );
	}
}

package ch.fhnw.richards.week08_ControlStructures.inClassExample;

import java.util.Scanner;

/**
 * The rooms are traversed in the order given. Goombas accumulate. For the examples:
 * We get 4 goombas, and need 3 to proceed. Then we get 2 goombas (total of 6) and need 5
 * to proceed. Finally, we get 1 goomba (total 7) and need either 7 or 8 to proceed.
 */
public class goombastacks {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numRooms = in.nextInt();
		boolean possible = true; // Set to false if we ever don't have enough Goombas
		long totalGoombas = 0; // Better be a long - numbers may get big
		for (int i = 0; i < numRooms; i++) {
			totalGoombas += in.nextInt();
			int numRequired = in.nextInt();
			if (numRequired > totalGoombas) {
				possible = false;
				break; // Actually a decent use for break...
			}
		}
		System.out.println( possible ? "possible" : "impossible");
		 
	}
}

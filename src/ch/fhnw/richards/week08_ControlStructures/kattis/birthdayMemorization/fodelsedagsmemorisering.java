package ch.fhnw.richards.week08_ControlStructures.kattis.birthdayMemorization;

import java.util.ArrayList;
import java.util.Scanner;

public class fodelsedagsmemorisering {
	private static ArrayList<Friend> friends = new ArrayList<>();

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numFriends = in.nextInt(); in.nextLine();
		
		// Read all friends
		for (int i = 0; i < numFriends; i++) {
			Friend friend = readFriend(in);
			addFriend(friend);
		}
		
		// Get names of all friends in list
		ArrayList<String> names = new ArrayList<String>();
		for (Friend f : friends) names.add(f.getName());
		
		sort(names);
		
		System.out.println(names.size());
		for (String name : names) System.out.println(name);
	}

	public static Friend readFriend(Scanner in) {
		String name = in.next();
		int like = in.nextInt();
		String birthday = in.next();
		String[] parts = birthday.split("/");
		int day = Integer.parseInt(parts[0]);
		int month = Integer.parseInt(parts[1]);
		return new Friend(like, name, month, day);
	}
	
	// If we find a friend with the same birthday
	// - Replace old friend, only if ID is higher
	// If no one found with same birthday, add to end of list
	private static void addFriend(Friend friend) {
		boolean foundBirthdate = false;
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).sameBirthday(friend)) {
				foundBirthdate = true;
				if (friend.getID() > friends.get(i).getID())
					friends.set(i, friend);
			}
		}
		if (!foundBirthdate) friends.add(friend);
	}
	
	// In-place selection sort for strings
	private static void sort(ArrayList<String> strings) {
		for (int i = 0; i < strings.size(); i++) {
			int smallest = i;
			for (int j = i+1; j < strings.size(); j++) {
				if (strings.get(j).compareTo(strings.get(smallest)) < 0) smallest = j;
			}
			String tmp = strings.get(smallest);
			strings.set(smallest, strings.get(i));
			strings.set(i, tmp);
		}
	}
}

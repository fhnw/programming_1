package ch.fhnw.richards.week08_ControlStructures.kattis.birthdayMemorization;

public class Friend {
	private final int ID; // Like's this friend this much
	private String name;
	private int month;
	private int day;
	
	public Friend(int ID, String name, int month, int day) {
		this.ID = ID;
		this.name = name;
		this.month = month;
		this.day = day;
	}
	
	public boolean sameBirthday(Friend f) {
		return this.month == f.month && this.day == f.day;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getID() {
		return ID;
	}
}

This solution is by Rainer Telesko

It includes a number of things "just for fun" - like small animations,
property listeners, and so forth. Your solution can be much simpler.
You just need to control the media player by calling methods like
"play", "pause" and "stop".
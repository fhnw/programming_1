package ch.fhnw.richards.week12_MoreProgramming.solutions.iPod;

import java.io.InputStream;
import java.net.URL;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.util.Duration;

// Pay attention to legal mp3-Downloads of songs

public class iPod extends Application {

	MediaPlayer mediaPlayer = null; // Initially no audio file selected and player running
	Media media = null;
	ComboBox<String> songListComboBox; // Combo Box for songs
	Button startButton, pauseButton, resumeButton, stopButton; // Buttons for starting, pausing, resuming and ending a
																// song
	Label selectedSongLbl; // Shows the selected song
	Label statusLbl; // Status Label
	Duration length; // Needed for pausing and resuming a song
	boolean isPaused; // Needed for pausing and resuming a song

	// Launch the application
	public void start(Stage stage) {

		// Songs for IPod
		String songs[] = { "Falco - Der Kommissar", "Opus - Flying High", "DJ Ötzi - Hey Baby" };

		// Create a combo box
		songListComboBox = new ComboBox<String>(FXCollections.observableArrayList(songs));

		// Slider for volume
		Slider volSlider;

		// Labels
		// Label to display the selected song
		selectedSongLbl = new Label("Song: Nothing selected");
		// Status Label with Animation
		statusLbl = new Label("Status: No song being played");
		animateStatusLabel();

		// Initialize Buttons (Buttons have Text and Icons)
		// Icons are from https://icons8.com/icons/
		InputStream start = getClass().getResourceAsStream("iconStart.png");
		Image startImage = new Image(start);
		ImageView startIv = new ImageView(startImage);
		startIv.setFitHeight(25);
		startIv.setFitWidth(25);

		InputStream pause = getClass().getResourceAsStream("iconPause.png");
		Image pauseImage = new Image(pause);
		ImageView pauseIv = new ImageView(pauseImage);
		pauseIv.setFitHeight(25);
		pauseIv.setFitWidth(25);

		InputStream resume = getClass().getResourceAsStream("iconResume.png");
		Image resumeImage = new Image(resume);
		ImageView resumeIv = new ImageView(resumeImage);
		resumeIv.setFitHeight(25);
		resumeIv.setFitWidth(25);

		InputStream stop = getClass().getResourceAsStream("iconStop.png");
		Image stopImage = new Image(stop);
		ImageView stopIv = new ImageView(stopImage);
		stopIv.setFitHeight(25);
		stopIv.setFitWidth(25);

		startButton = new Button("Start");
		startButton.setGraphic(startIv);

		pauseButton = new Button("Pause");
		pauseButton.setGraphic(pauseIv);

		resumeButton = new Button("Resume");
		resumeButton.setGraphic(resumeIv);

		stopButton = new Button("Stop");
		stopButton.setGraphic(stopIv);

		// Initialize Volume Slider
		// Setting the preferences for volume Slider
		volSlider = new Slider(0, 100, 0);
		volSlider.setValue(50);
		volSlider.setShowTickLabels(true);
		volSlider.setShowTickMarks(true);
		volSlider.setMajorTickUnit(25);
		volSlider.setBlockIncrement(10);

		// Create a VBox
		VBox iPodVBox = new VBox();
		iPodVBox.setSpacing(30);
		iPodVBox.setPadding(new Insets(5, 5, 5, 5));
		iPodVBox.setAlignment(Pos.CENTER);
		iPodVBox.getChildren().addAll(songListComboBox, selectedSongLbl, statusLbl, startButton, pauseButton,
				resumeButton, stopButton, volSlider);
		iPodVBox.setStyle("-fx-background-color: #FFFF99;"); // Setting the background color to light yellow

		// Create action event showing the selected song
		EventHandler<ActionEvent> showSong = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				selectedSongLbl.setText("Song: " + songListComboBox.getValue() + " selected");
			}
		};

		// Providing event listener for Combo Box
		songListComboBox.setOnAction(showSong);

		// Providing event listener for Slider
		volSlider.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> oberservable, Number oldValue, Number newValue) {
				mediaPlayer.setVolume(volSlider.getValue() / 100); // Setting the volume as specified by the user
			}
		});

		// Event Handler for all four Buttons
		startButton.setOnAction(this::processStart);
		pauseButton.setOnAction(this::processPause);
		resumeButton.setOnAction(this::processResume);
		stopButton.setOnAction(this::processStop);

		// Create a scene
		Scene scene = new Scene(iPodVBox, 300, 500);

		// Set title for the stage
		stage.setTitle("iPod Simulation");

		// Set the scene
		stage.setScene(scene);

		// Change Favicon for the App
		// Download from https://icons8.de/icons
		stage.getIcons().add(new Image(getClass().getResourceAsStream("iconMusic.png")));
		stage.show();
	}

	public static void main(String args[]) {

		launch(args);
	}

	private void processStart(ActionEvent event) {
		if (mediaPlayer != null) // Stop any currently running song
			mediaPlayer.stop();

		if (songListComboBox.getValue() != null)
			switch (songListComboBox.getValue()) {
			case "Falco - Der Kommissar":
				playSong("DerKommissar.mp3");
				break;
			case "Opus - Flying High":
				playSong("FlyingHigh.mp3");
				break;
			case "DJ Ötzi - Hey Baby":
				playSong("HeyBaby.mp3");
				break;
			}
		else {
			statusLbl.setText("Status: Please select song for playing");
		}
	}

	private void playSong(String song) {
		URL resource = getClass().getResource(song);
		media = new Media(resource.toString());
		mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE); // Endless playing of a selected song
		mediaPlayer.play();
		statusLbl.setText("Status: " + song + " " + "is being played");
	}

	private void animateStatusLabel() {
		FadeTransition fadeTransition = new FadeTransition(Duration.seconds(3.0), statusLbl);
		fadeTransition.setFromValue(1.0);
		fadeTransition.setToValue(0.0);
		fadeTransition.setCycleCount(Animation.INDEFINITE);
		fadeTransition.play();
	}

	private void processStop(ActionEvent event) {
		if (mediaPlayer != null) {
			mediaPlayer.stop();
			statusLbl.setText("Status: Music stopped");
			mediaPlayer = null;
		} else
			statusLbl.setText("Status: Nothing to stop");
	}

	private void processPause(ActionEvent event) {
		if (mediaPlayer != null) {
			mediaPlayer.pause();
			isPaused = true; // Flag set to true
			length = mediaPlayer.getCurrentTime();
			statusLbl.setText("Status: Music paused");
		} else
			statusLbl.setText("Status: Nothing to pause");
	}

	private void processResume(ActionEvent event) {
		if (mediaPlayer != null && isPaused) { // Flag checked
			mediaPlayer.seek(length);
			mediaPlayer.play();
			statusLbl.setText("Status: Music resumed");
			isPaused = false;
		} else
			statusLbl.setText("Status: Nothing to resume");
	}

}
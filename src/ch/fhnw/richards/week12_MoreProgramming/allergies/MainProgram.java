package ch.fhnw.richards.week12_MoreProgramming.allergies;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * This sample solution uses a console interface. There is no error-checking,
 * so the user cannot make any mistakes.
 * 
 * Feel free to write a JavaFX interface!
 */
public class MainProgram {
	private static AllergyLog allergyLog = new AllergyLog(); // our business data

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String command = "X"; // anything but 'Q'

		while (command.charAt(0) != 'Q') {
			System.out.println("What would you like to do? (A)dd an entry, (P)rint a report or (Q)uit?");
			command = in.nextLine();
			if (command.charAt(0) == 'A') addEntry(in);
			else if (command.charAt(0) == 'P') printReport();
			else if (command.charAt(0) != 'Q') System.out.println("Invalid command");
		}
		System.out.println("Goodbye!");
	}

	private static void addEntry(Scanner in) {
		System.out.println("Enter the date of the entry: dd mm yyyy");
		int dd = in.nextInt(); int mm = in.nextInt(); int yyyy = in.nextInt(); in.nextLine();
		LogEntry entry = new LogEntry(LocalDate.of(yyyy, mm, dd));
		
		// We use lots of little methods - keep each method simple
		entry.setRunnyNose(readSeverity(in, "Runny nose"));
		entry.setItchySkin(readSeverity(in, "Itchy skin"));
		entry.setRedEyes(readSeverity(in, "Red eyes"));
		
		entry.setLocation(readLocation(in));
		entry.setFood(readFood(in));
		
		allergyLog.addEntry(entry);
	}
	
	private static Severity readSeverity(Scanner in, String symptom) {
		System.out.println(symptom + ": how severe this symptom? (N)one, (M)ild, (S)evere, (H)orrible?");
		String s = in.nextLine();
		Severity severity;
		switch (s.charAt(0)) {
			case 'M' -> severity = Severity.Mild;
			case 'S' -> severity = Severity.Severe;
			case 'H' -> severity = Severity.Horrible;
			default -> severity = Severity.None;
		}
		return severity;
	}
	
	private static Location readLocation(Scanner in) {
		System.out.println("Enter your location (H)ome, (S)chool, (W)ork, (O)ther");
		String s = in.nextLine();
		Location location;
		switch (s.charAt(0)) {
			case 'H' -> location = Location.Home;
			case 'S' -> location = Location.School;
			case 'W' -> location = Location.Work;
			default -> location = Location.Other;
		}
		System.out.println("Enter any details about the location");
		location.setDetails(in.nextLine());
		return location;
	}

	private static String readFood(Scanner in) {
		System.out.println("What did you eat today?");
		return in.nextLine();
	}
	
	private static void printReport() {
		String report = allergyLog.generateReport();
		System.out.println(report);
	}
	
}

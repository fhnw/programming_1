package ch.fhnw.richards.week12_MoreProgramming.allergies;

public enum Severity {
	None, Mild, Severe, Horrible;
}

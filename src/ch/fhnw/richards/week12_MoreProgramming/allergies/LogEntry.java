package ch.fhnw.richards.week12_MoreProgramming.allergies;

import java.time.LocalDate;
import java.util.ArrayList;

public class LogEntry {
	private LocalDate date; // This is the ID, and cannot be changed
	private Severity runnyNose;
	private Severity itchySkin;
	private Severity redEyes;
	private Location location;
	private String food;

	public LogEntry(LocalDate date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return date + " " + location + "(" + location.getDetails() + ") " + food;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;                          // Let's be pessimists
		if (obj != null) {                               // If parameter is not null
			if (this.getClass() == obj.getClass()) {     // If parameter is our class
				LogEntry other = (LogEntry) obj;         // Cast parameter to our class
				result = (this.date.equals(other.date)); // Compare the ID values
			}
		}
		return result;
	}

	// --- Getters and Setters ---

	public Severity getRunnyNose() {
		return runnyNose;
	}

	public void setRunnyNose(Severity runnyNose) {
		this.runnyNose = runnyNose;
	}

	public Severity getItchySkin() {
		return itchySkin;
	}

	public void setItchySkin(Severity itchySkin) {
		this.itchySkin = itchySkin;
	}

	public Severity getRedEyes() {
		return redEyes;
	}

	public void setRedEyes(Severity redEyes) {
		this.redEyes = redEyes;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}

	public LocalDate getDate() {
		return date;
	}
}

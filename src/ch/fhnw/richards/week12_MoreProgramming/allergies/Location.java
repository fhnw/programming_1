package ch.fhnw.richards.week12_MoreProgramming.allergies;

public enum Location {
	Home(""), School(""), Work(""), Other("");
	private String details;
	
	private Location(String details) {
		this.details = details;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}

package ch.fhnw.richards.week12_MoreProgramming.allergies;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * This class contains the business logic functions: adding, getting and removing
 * entries, and generating the report.
 */
public class AllergyLog {
	private ArrayList<LogEntry> logEntries = new ArrayList<>();

	/**
	 * Add a new entry to our list.
	 */
	public void addEntry(LogEntry entry) {
		logEntries.add(entry);
	}

	/**
	 * Get an entry for a particular date
	 * @return the requested entry, or null if not found
	 */
	public LogEntry getEntry(LocalDate date) {
		LogEntry entry = null;
		for (LogEntry e : logEntries) {
			if (e.getDate().equals(date)) {
				entry = e;
				break; // found, so we can stop
			}
		}
		return entry;
	}
	
	/**
	 * Remove an entry for a particular date
	 */
	public void removeEntry(LocalDate date) {
		LogEntry temp = new LogEntry(date); // Create an entry with the ID we are looking for
		logEntries.remove(temp); // Remove the entry that matches; if nothing matches, nothing happens
	}
	
	/**
	 * Generate a report: A string containing information from all days where any sympton was severe or horrible.
	 * When building complicated strings, StringBuffer is a useful class
	 * @return A string containing the information
	 */
	public String generateReport() {
		StringBuffer sb = new StringBuffer();
		for (LogEntry e : logEntries) {
			if (e.getRunnyNose().compareTo(Severity.Severe) >= 0
					|| e.getRedEyes().compareTo(Severity.Severe) >= 0
					|| e.getItchySkin().compareTo(Severity.Severe) >= 0) {
				sb.append(e.toString());
				sb.append("\n");
			}
		}
		return sb.toString();
	}
}

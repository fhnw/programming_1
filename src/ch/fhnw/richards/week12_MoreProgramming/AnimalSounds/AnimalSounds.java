package ch.fhnw.richards.week12_MoreProgramming.AnimalSounds;

import java.io.InputStream;
import java.net.URL;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.event.ActionEvent;
import javafx.stage.Stage;

public class AnimalSounds extends Application {
	private Button btnDog;
	private Button btnCat;
	private MediaPlayer mediaPlayer;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Set up the dog button (step-by-step)
		btnDog = new Button();
		InputStream is = getClass().getResourceAsStream("dog.png");
		Image dogImage = new Image(is);
		ImageView dogIv = new ImageView(dogImage);
		btnDog.setGraphic(dogIv);

		// Set up the cat button (shorter approach)
		btnCat = new Button();
		btnCat.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("cat.png"))));
		
		VBox root = new VBox(btnDog, btnCat);
		root.setId("root");		
		
		// Event handling
		btnDog.setOnAction(this::woof);
		btnCat.setOnAction(this::meow);
		
		// Create the scene and attach the stylesheet
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("AnimalSounds.css").toExternalForm());
		
		// Set the title and icon for the window
		primaryStage.setTitle("Animal Sounds"); // Title of the window
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("paw.png")));
		
		// Set the scene and show the stage
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void woof(ActionEvent e) {
		// Play the dog's woof (step-by-step)
		URL resource = getClass().getResource("woof.mp3");
		Media media = new Media(resource.toString());
		mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
	}
	
	private void meow(ActionEvent e) {
		// Play the cat's meow (shorter approach)
		mediaPlayer = new MediaPlayer(new Media(getClass().getResource("meow.mp3").toString()));
		mediaPlayer.play();
	}
}
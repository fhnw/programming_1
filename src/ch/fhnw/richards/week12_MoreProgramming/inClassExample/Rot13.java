package ch.fhnw.richards.week12_MoreProgramming.inClassExample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Rot13 extends Application {
	private Label lblInstructions = new Label("Enter text to encode or decode");
	private TextField txtContent = new TextField();
	private Button btnEncode = new Button("Encode/Decode.");

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		VBox root = new VBox(20);
		root.getChildren().add(lblInstructions);
		root.getChildren().add(txtContent);
		root.getChildren().add(btnEncode);
		
		btnEncode.setOnAction(this::encode);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Rot13 Encode/Decode");
		stage.show();
	}
	
	private void encode(ActionEvent e) {
		String text = txtContent.getText();
		char[] chars = text.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] >= 'A' && chars[i] < 'Z') {
				chars[i] += 13;
				if (chars[i] > 'Z') chars[i] -= 26;
			} else if (chars[i] >= 'a' && chars[i] < 'z') {
				chars[i] += 13;
				if (chars[i] > 'z') chars[i] -= 26;
			} else {
				// No change for other characters
			}
		}
		text = new String(chars);
		txtContent.setText(text);
	}
}

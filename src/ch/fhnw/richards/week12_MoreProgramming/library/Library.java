package ch.fhnw.richards.week12_MoreProgramming.library;

import java.util.ArrayList;

public class Library {
	private ArrayList<Client> clients;
	private ArrayList<Book> books;
	
	public Library() {
		this.clients = new ArrayList<>();
		this.books = new ArrayList<>();
	}
	
	public void buyBook(String ISBN, int quantity) {
		// Use the normal constructor
		Book book = new Book(ISBN);
		// Here there should be code to fill in author, title, etc.
		books.add(book);
		
		// Use the copy-constructor
		for (int i = 2; i <= quantity; i++) {
			Book nextCopy = new Book(book);
			books.add(book);			
		}
	}
	
	public boolean checkout(Book book, Client client) {
		boolean success = false;
		if (book.getClient() == null) {
			book.setClient(client);
			client.addBook(book);
			success = true;
		}
		return success;
	}
	
	public boolean checkin(Book book, Client client) {
		boolean success = false;
		if (book.getClient().equals(client)) {
			if (client.removeBook(book)) {
				book.setClient(null);
				success = true;
			}
		}
		return success;
	}
}

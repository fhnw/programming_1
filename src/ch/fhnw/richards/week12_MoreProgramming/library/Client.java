package ch.fhnw.richards.week12_MoreProgramming.library;

import java.util.ArrayList;

public class Client {
	private static int nextID = 0;
	
	private final int ID;
	private String name;
	private String email;
	private ArrayList<Book> books;
	
	public Client(String name, String email) {
		this.ID = nextID++;
		this.name = name;
		this.email = email;
		this.books = new ArrayList<>();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Client c = (Client) o;
		return this.ID == c.ID;
	}
	
	/**
	 * Add a new book to the list of checked-out books
	 */
	public void addBook(Book book) {
		books.add(book);
	}
	
	/**
	 * @return true if book could be removed
	 */
	public boolean removeBook(Book book) {
		boolean found = false;
		int pos = 0;
		while (!found && pos < books.size()) {
			Book b = books.get(pos);
			if (b.equals(book)) {
				books.remove(pos);
				found = true;
			}
			pos++;
		}
		return found;
	}
}

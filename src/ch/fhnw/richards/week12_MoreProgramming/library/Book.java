package ch.fhnw.richards.week12_MoreProgramming.library;

public class Book {
	private final String ISBN;
	private String author;
	private String title;
	private String publisher;
	private Client client;
	
	public Book(String ISBN) {
		this.ISBN = ISBN;
	}
	
	public Book(String ISBN, String author, String title, String publisher) {
		this.ISBN = ISBN;
		this.author = author;
		this.title = title;
		this.publisher = publisher;
	}
	
	// Copy constructor - make an exact copy, except for the client
	public Book(Book book) {
		this.ISBN = book.ISBN;
		this.author = book.author;
		this.title = book.title;
		this.publisher = book.publisher;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Book b = (Book) o;
		return this.ISBN.equals(b.ISBN);
	}
	
	// --- Getters and Setters ---

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public String getISBN() {
		return ISBN;
	}
}
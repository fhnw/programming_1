package ch.fhnw.richards.week14_ExamPrep.slidingPuzzle;

import java.util.ArrayList;
import java.util.Collections;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * This program implements the classic "sliding puzzle", in a 3x3 grid. See
 * http://mypuzzle.org/sliding for an example.
 * 
 * ==> This is just a program skeleton, to get you started! <==
 */
public class SlidingPuzzle extends Application implements EventHandler<ActionEvent> {
	int[][] values; // The arrangement of values (0 = blank)
    Button[][] buttons = new Button[3][3]; // The buttons displaying the values

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane root = new GridPane();

        // Create 3x3 array of numbers, randomly arranged.
        values = createNumberArray();

        // Create the buttons and add them to the array and the GUI, setting the text for each
        // button from the values. Also register an event handler for each button. All buttons use
        // the same event-handling method, implemented in this class.
        // TODO

        // Update the texts displayed in the buttons
        // TODO

        // Create the scene using our layout; then display it
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        primaryStage.setTitle("Sliding Puzzle");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Create a 3x3 array of integers, containing the values 0 to 8, randomly arranged.
     * 
     * There are many ways to do this, but you may find the method "Collections.shuffle()" to be
     * useful; it can be applied to an ArrayList.
     */
    private int[][] createNumberArray() {
        // TODO
    	return null;
    }

    /**
     * // Update the texts displayed in the buttons: Set the text on each button to the
     * corresponding value
     */
    private void updateButtonTexts() {
        // TODO
    }

    /**
     * Method to handle the button events
     */
	@Override
	public void handle(ActionEvent event) {
		// TODO Auto-generated method stub
		
	}
}
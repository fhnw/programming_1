package ch.fhnw.richards.week14_ExamPrep.solutions.sportsTeams;

import java.time.LocalDate;
import java.util.Scanner;

public class TeamManager {

	public static void main(String[] args) {
		Team team = new Team("Bullfrogs");
		Scanner in = new Scanner(System.in);
		
		int command = 0;
		while (command != 5) {
			System.out.println("Command: 1 = Team info, 2 = Average age \n" +
							"3 = Add player, 4 = Remove player, 5 = Quit");
			command = in.nextInt();
			in.nextLine(); // Discard end-of-line character
			
			switch(command) {
			case 1 -> System.out.println(team);
			case 2 -> System.out.println("Average age of players is " + team.getAverageAge());
			case 3 -> team.addPlayer(readPlayer(in));
			case 4 -> team.removePlayer(readPlayerID(in));
			case 5 -> System.out.println("Quitting...");
			default -> System.out.println("Unknown command, please try again");
			}
		}
	}

	private static Player readPlayer(Scanner in) {
		System.out.println("Enter player name");
		String name = in.nextLine();
		
		// Birthday
		System.out.println("Enter player birthday as three integers: year month day");
		int year = in.nextInt(); int month = in.nextInt(); int day = in.nextInt();
		in.nextLine(); // Discard end-of-line character
		LocalDate birthday = LocalDate.of(year, month, day);
		
		// Position - must be read as String (or maybe int), and converted
		System.out.print("Enter the position: ");
		for (Position p : Position.values()) System.out.print(p + " ");
		System.out.println();
		String pos_as_string = in.nextLine();
		Position position = Position.Center;
		if (pos_as_string.charAt(0) == 'F') position = Position.Fullback;
		else if (pos_as_string.charAt(0) == 'G') position = Position.GoalKeeper;
		else if (pos_as_string.charAt(0) == 'S') position = Position.Striker;
		
		return new Player(name, birthday, position);
	}
	
	private static int readPlayerID(Scanner in) {
		System.out.println("Enter ID of player to remove");
		int ID = in.nextInt();
		in.nextLine(); // Discard end-of-line character
		return ID;
	}
}

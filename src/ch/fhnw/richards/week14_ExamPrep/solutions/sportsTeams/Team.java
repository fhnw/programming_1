package ch.fhnw.richards.week14_ExamPrep.solutions.sportsTeams;

import java.util.ArrayList;

public class Team {
	private static int nextID = 0;
	
	private final int ID;
	private String name;
	private final ArrayList<Player> players;
	
	public int getNumPlayers() {
		return players.size();
	}
	
	public double getAverageAge() {
		double answer = 0;
		for (Player p : players) {
			answer += p.getAge();
		}
		answer /= players.size();
		return answer;
	}
	
	public void addPlayer(Player p) {
		players.add(p);
	}
	
	/**
	 * This method would be simpler, if we used an Iterator
	 */
	public void removePlayer(int ID) {
		boolean found = false;
		for (int i = 0; i < players.size() && !found; i++) {
			if (players.get(i).getID() == ID) {
				players.remove(i);
				found = true;
			}
		}
	}
	
	@Override
	public String toString() {
		String out = name + "( " + players.size() + " players) ";
		for (Player p : players) {
			out += "\n" + p.toString();
		}
		return out;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getID() {
		return ID;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public Team(String name) {
		this.ID = nextID++;
		this.name = name;
		this.players = new ArrayList<>();
	}

	
}

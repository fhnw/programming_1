package ch.fhnw.richards.week14_ExamPrep.solutions.sportsTeams;

import java.time.LocalDate;
import java.time.Period;

public class Player {
	private static int nextID = 0;

	private final int ID;
	private String name;
	private LocalDate birthday;
	private Position position;

	public Player(String name, LocalDate birthday, Position position) {
		this.ID = nextID++;
		this.name = name;
		this.birthday = birthday;
		this.position = position;
	}

	/**
	 * You could also calculate the age directly, using year, month and day of the
	 * two dates. Using Period is easier...
	 */
	public int getAge() {
		LocalDate today = LocalDate.now();
		Period p = Period.between(birthday, today);
		return p.getYears();
	}

	@Override
	public String toString() {
		return name + " (" + ID + ") plays " + position;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getID() {
		return ID;
	}

}

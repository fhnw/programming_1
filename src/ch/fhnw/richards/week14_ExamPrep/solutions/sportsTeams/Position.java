package ch.fhnw.richards.week14_ExamPrep.solutions.sportsTeams;

public enum Position {
	GoalKeeper, Fullback, Center, Striker
}

package ch.fhnw.richards.week14_ExamPrep.solutions.slidingPuzzle;

import java.util.ArrayList;
import java.util.Collections;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * This program implements the classic "sliding puzzle", in a 3x3 grid. See
 * http://mypuzzle.org/sliding for an example.
 */
public class SlidingPuzzle extends Application implements EventHandler<ActionEvent> {
    int[][] values; // The arrangement of values (0 = blank)
    Button[][] buttons = new Button[3][3]; // The buttons displaying the values

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane root = new GridPane();

        // Create 3x3 array of numbers, randomly arranged.
        values = createNumberArray();

        // Create the buttons and add them to the array and the GUI, setting the text for each
        // button from the values. Also register an event handler for each button. All buttons use
        // the same event-handling method, implemented in this class.
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {
                Button b = new Button(Integer.toString(values[col][row]));
                root.add(b, col, row);
                buttons[col][row] = b;
                b.setOnAction(this);
            }
        }

        // Update the texts displayed in the buttons
        updateButtonTexts();

        // Create the scene using our layout; then display it
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        primaryStage.setTitle("Sliding Puzzle");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Method to handle the button events
     */
    @Override
    public void handle(ActionEvent event) {
        // locate button
        int bcol = -1, brow = -1;
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {
                if (buttons[col][row] == event.getSource()) {
                    bcol = col;
                    brow = row;
                }
            }
        }

        // working with the values grid, see if the button is next to the space (0). If so, swap the
        // two values. There are only four possibilities, so we check each one explicitly
        if (bcol > 0 && values[bcol - 1][brow] == 0) { // blank to the left
            values[bcol - 1][brow] = values[bcol][brow];
            values[bcol][brow] = 0;
        } else if (bcol < 2 && values[bcol + 1][brow] == 0) { // blank to the right
            values[bcol + 1][brow] = values[bcol][brow];
            values[bcol][brow] = 0;
        } else if (brow > 0 && values[bcol][brow - 1] == 0) { // blank above
            values[bcol][brow - 1] = values[bcol][brow];
            values[bcol][brow] = 0;
        } else if (brow < 2 && values[bcol][brow + 1] == 0) { // blank below
            values[bcol][brow + 1] = values[bcol][brow];
            values[bcol][brow] = 0;
        }

        // Update the texts displayed in the buttons
        updateButtonTexts();
    }

    /**
     * Create a 3x3 array of integers, containing the values 0 to 8, randomly arranged.
     * 
     * There are many ways to do this, but you may find the method "Collections.shuffle()" to be
     * useful; it can be applied to an ArrayList.
     */
    private int[][] createNumberArray() {
        int[][] valueGrid = new int[3][3];
        ArrayList<Integer> values = new ArrayList<>();
        for (int i = 0; i <= 8; i++)
            values.add(i);
        Collections.shuffle(values);
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {
                valueGrid[col][row] = values.get(col * 3 + row);
            }
        }
        return valueGrid;
    }

    /**
     * // Update the texts displayed in the buttons: Set the text on each button to the
     * corresponding value
     */
    private void updateButtonTexts() {
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {
                int value = values[col][row];
                if (value == 0) {
                    buttons[col][row].setText("");
                } else {
                    buttons[col][row].setText(Integer.toString(value));
                }
            }
        }
    }
}

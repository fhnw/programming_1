package ch.fhnw.richards.week14_ExamPrep.solutions.numerology;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Numerology extends Application implements EventHandler<ActionEvent> {
	Label lblName = new Label("Full name");
	private TextField txtName = new TextField();
	private Button btnCalculate = new Button("Calculate");
	private Label lblMeaning = new Label();

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox nameRow = new HBox(lblName, txtName, btnCalculate);
		VBox root = new VBox(nameRow, lblMeaning);

		lblMeaning.setWrapText(true);

		btnCalculate.setOnAction(this);

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root, 500, 150);
		scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		primaryStage.setTitle("Sliding Puzzle");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	@Override
	public void handle(ActionEvent event) {
		// Calculate the numerological value
		int value = numerologicalValue(txtName.getText());

		// Fetch description and display result
		lblMeaning.setText("Value " + value + " means: " + describe(value));
	}

	/**
	 * Step 1: Convert letter A-Z to numeric value; all other characters receive a
	 * value of zero.
	 */
	private int letterValue(char letter) {
		int value = 0;
		if (letter >= 'A' && letter <= 'Z') {
			value = (letter - 'A') % 9 + 1;
		}
		return value;
	}

	/**
	 * Step 2: Process the letters A-Z. We convert letters a-z to upper case at this
	 * point. We return an int[] holding all of the values
	 */
	private int[] letterValues(char[] letters) {
		int[] values = new int[letters.length];
		for (int i = 0; i < letters.length; i++) {
			char c = letters[i];
			c = Character.toUpperCase(c);
			values[i] = letterValue(c);
		}
		return values;
	}

	/**
	 * Step 3: Sum the digits of an integer
	 */
	private int sumOfDigits(int in) {
		int sum = 0;
		while (in > 0) {
			sum += in % 10;
			in = in / 10;
		}
		return sum;
	}

	/**
	 * Step 4: Calculate numerological value of a string
	 */
	private int numerologicalValue(String name) {
		char[] chars = name.toCharArray();
		int[] values = letterValues(chars);

		// Sum the values
		int sum = 0;
		for (int value : values)
			sum += value;

		// Reduce to one of the final values
		while (sum > 23 || (sum > 11 && sum < 22) || sum == 10) {
			sum = sumOfDigits(sum);
		}

		return sum;
	}

	/**
	 * Step 5: Convert values into silly descriptions
	 */
	private String describe(int sum) {
		String desc;
		switch (sum) {
		case 1:
			desc = "One";
			break;
		case 2:
			desc = "Two";
			break;
		case 3:
			desc = "Three";
			break;
		case 4:
			desc = "Four";
			break;
		case 5:
			desc = "Five";
			break;
		case 6:
			desc = "Six";
			break;
		case 7:
			desc = "Seven";
			break;
		case 8:
			desc = "Eight";
			break;
		case 9:
			desc = "Nine";
			break;
		case 11:
			desc = "You consider yourself a born leader, but others think you're an idiot. You are vain and cannot tolerate honest criticism. You recklessly rely on luck, since you have no talent. Most elevens are murdered.";
			break;
		case 22:
			desc = "Twenty two";
			break;
		default:
			desc = "I know nothing about you!";
		}
		return desc;
	}
}

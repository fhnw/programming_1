package ch.fhnw.richards.xtra.javaFX_examples;

import java.awt.Point;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class SimpleCanvas extends Application {
    private boolean mouseIsPressed = false;
    private Point2D pressedPoint;
    
    // Drawing modes (would be better as enumeration)
    private boolean followMouseMode = true; // default
    private boolean drawCircleMode = false;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Simple Canvas");
        
        BorderPane root = new BorderPane();
        MenuBar menuBar = new MenuBar();
        root.setTop(menuBar);
        createMenus(menuBar);
        
        Canvas canvas = new Canvas(400, 400);
        root.setCenter(canvas);
        createMouseEvents(canvas);
        
        // register ourselves to handle window-closing event
        stage.setOnCloseRequest( e -> {
                // This would be a good place to ask the
                // user if they want to save their drawing
                Platform.exit();
        });

        Scene scene = new Scene(root);
        scene.getStylesheets().add(
                getClass().getResource("ButtonClick.css").toExternalForm());
        stage.setScene(scene);;
        stage.show();
    }
    
    private void createMenus(MenuBar menuBar) {
        Menu drawMenu = new Menu("Draw");
        Menu helpMenu = new Menu("Help");
        menuBar.getMenus().addAll(drawMenu, helpMenu);
        
        MenuItem followMouse = new MenuItem("Follow mouse");
        followMouse.setOnAction( e ->  {
                followMouseMode = true;
                drawCircleMode = false;
        });
        
        MenuItem drawCircle = new MenuItem("Draw circle");
        drawCircle.setOnAction( e -> {
                followMouseMode = false;
                drawCircleMode = true;
        });
        
        drawMenu.getItems().addAll(followMouse, drawCircle);
    }
    
    private void createMouseEvents(Canvas canvas) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        canvas.setOnMousePressed( e ->  {
                mouseIsPressed = true;
                pressedPoint = new Point2D(e.getX(), e.getY());
                if (followMouseMode) {
                    gc.setStroke(Color.GREEN);
                    gc.strokeOval(e.getX(), e.getY(), 3, 3);
                }
        });
        canvas.setOnMouseReleased( e -> {
                mouseIsPressed = false;
                if (followMouseMode) {
                    gc.setStroke(Color.RED);
                    gc.strokeOval(e.getX(), e.getY(), 3, 3);
                } else {
                    double width = e.getX() - pressedPoint.getX();
                    double height = e.getY() - pressedPoint.getY();
                    gc.setStroke(Color.BLUE);
                    gc.strokeOval(pressedPoint.getX(), pressedPoint.getY(), width, height);
                }
        });
        canvas.setOnMouseDragged( e -> {
                if (mouseIsPressed & followMouseMode) {
                    gc.setStroke(Color.BLACK);
                    gc.strokeOval(e.getX(), e.getY(), 1, 1);
                }
        });
    }
}

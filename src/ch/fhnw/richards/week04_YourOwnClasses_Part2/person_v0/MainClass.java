package ch.fhnw.richards.week04_YourOwnClasses_Part2.person_v0;

import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter first name and last name");
		String firstName = in.next();
		String lastName = in.next();
		System.out.println("Enter the person's gender (m or f)");
		String genderString = in.next(); // We will just look at the first character
		in.close();

		int gender = 1;
		if (genderString.charAt(0) == 'm') gender = 0;
		
		Person p = new Person(0, firstName, lastName);
		p.setGender(gender);
		
		String description = p.toString();
		System.out.println(description);
	}
}

package ch.fhnw.richards.week04_YourOwnClasses_Part2.dice;

import java.util.Random;

public class Dice {
	private static Random random = new Random();
	private int numberOfSides;
	
	public Dice(int n) {
		numberOfSides = n;
	}
	
	// A different constructor
	// for the most common case
	public Dice() {
		numberOfSides = 6;
	}
	
	public int getNumberOfSides() {
		return numberOfSides;
	}
	
	public void setNumberOfSides(int numberOfSides) {
		this.numberOfSides = numberOfSides;
	}
	
	public double theoreticalAverage() {
		double average = (1.0 + numberOfSides) / 2.0;
		return average;
	}
	
	public int rollDice() {
		int value = random.nextInt(numberOfSides) + 1;
		return value;
	}
}

package ch.fhnw.richards.week04_YourOwnClasses_Part2.dice;

import java.util.Scanner;

public class SimulateDice {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("What size of dice should we use?");
		int sizeOfDice = in.nextInt();
		in.close();
		
		// Create object of class Dice
		Dice dice = new Dice(sizeOfDice);
		
		// Roll dice three times, calculate average
		double actualAverage = 0.0;
		actualAverage += dice.rollDice();
		actualAverage += dice.rollDice();
		actualAverage += dice.rollDice();
		actualAverage /= 3;
		
		// Give the user the information
		System.out.println("The average should be " + dice.theoreticalAverage());
		System.out.println("The average of three rolls was " + actualAverage);
	}
}

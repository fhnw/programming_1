package ch.fhnw.richards.week04_YourOwnClasses_Part2.person_v1;

public class Person {
	private static int nextID = 0;
	
	private final int ID; // ID unchangeable after being set
	private String lastName;
	private String firstName;
	private int gender;
	
	private static int getNextID() {
		return nextID++;
	}

	public Person() {
		this.ID = getNextID();
	}

	public Person(String lastName, String firstName) {
		this.ID = getNextID();
		this.lastName = lastName;
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		String description = this.ID + ": " + this.lastName + ", " + this.firstName;
		if (gender == 0)
			description += " (male)";
		else
			description += " (female)";
		return description;
	}
	
	// Getters and setters
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (!lastName.isEmpty()) this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (!firstName.isEmpty()) this.firstName = firstName;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getID() {
		return ID;
	}
}

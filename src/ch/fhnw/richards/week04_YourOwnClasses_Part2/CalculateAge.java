package ch.fhnw.richards.week04_YourOwnClasses_Part2;

import java.time.LocalDate;
import java.util.Scanner;

public class CalculateAge {

	public static void main(String[] args) {
		int birthYear = readBirthYear();
	    int age = calcAge(birthYear);
	    
	    if (age < 0)
	    	System.out.println("Oops, did you enter the right birth year?");
	    else
	    	System.out.println("You are " + age + " years old");
	}
	
	private static int readBirthYear() {
		// Communicate with the user
	    Scanner in = new Scanner(System.in);
	    System.out.println("Enter your birth year");
	    int birthYear = in.nextInt();
	    in.close();
	    
	    // Return the user input
	    return birthYear;
	}
	
	/**
	 * This method calculates a person's age, based on their year of
	 * birth and today's date. The result is not always correct,
	 * since it does not account for the month and day.
	 */
	private static int calcAge(int birthYear) {
	    // Get the current year
	    LocalDate today = LocalDate.now();
	    int thisYear = today.getYear();

	    // Subtract, to get age
	    int age = thisYear - birthYear;

	    // return result
	    return age;
	}
}

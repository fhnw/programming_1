package ch.fhnw.richards.week04_YourOwnClasses_Part2.dogs;

import java.time.LocalDate;
import java.time.Period;

public class SimpleDog {
	public String name;
	public String breed; // probably should be a class
	public LocalDate birthDate;

	public float getAgeInYears() {
		LocalDate today = LocalDate.now();
		Period result = Period.between(birthDate, today);
		return result.getYears();
	}
}
package ch.fhnw.richards.week04_YourOwnClasses_Part2.dogs;

import java.time.LocalDate;
import java.time.Period;

public class BetterDog {
	private int id;
	private String name;
	private String breed;
	public LocalDate birthDate;

	public BetterDog(int id) {
		this.id = id;
	}

	public float getAgeInYears() {
		LocalDate today = LocalDate.now();
		Period result = Period.between(birthDate, today);
		return result.getYears();
	}

	// Getters and setters
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (!name.isEmpty())
			this.name = name;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		if (!breed.isEmpty())
			this.breed = breed;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		if (birthDate.isBefore(LocalDate.now())) this.birthDate = birthDate;
	}
}
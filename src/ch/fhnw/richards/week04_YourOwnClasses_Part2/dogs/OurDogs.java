package ch.fhnw.richards.week04_YourOwnClasses_Part2.dogs;

import java.time.LocalDate;
import java.util.Scanner;

public class OurDogs {

	public static void main(String[] args) {
		BetterDog firstDog = new BetterDog(1);
		firstDog.setName("Polo");
		firstDog.setBreed("Oops");
		firstDog.setBirthDate(LocalDate.of(2003, 04, 11));
		
		BetterDog secondDog = new BetterDog(2);
		secondDog.setName("Sly");
		secondDog.setBreed("Kelpie");
		secondDog.setBirthDate(LocalDate.of(2003, 04, 18));

		Scanner in = new Scanner(System.in);
		System.out.println("Which dog do you want to know about (1 or 2)");
		int which = in.nextInt();
		in.close();
		
		if (which == 1) {
			System.out.println(firstDog.getName() + " is " + firstDog.getAgeInYears() + " years old");
		} else if (which == 2) {
			System.out.println(secondDog.getName() + " is " + secondDog.getAgeInYears() + " years old");
		} else {
			System.out.println("No such dog!");
		}
	}
}

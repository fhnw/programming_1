package ch.fhnw.richards.week04_YourOwnClasses_Part2.solutions.codeRunner.Books;

import java.util.Scanner;

public class BookTestProgram {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the ISBN");
		String ISBN = in.nextLine();
		System.out.println("Enter the author");
		String author = in.nextLine();
		System.out.println("Enter the title");
		String title = in.nextLine();
		System.out.println("Enter the number of pages");
		int numPages = in.nextInt();
		in.close();
		
		// Create a book object and set the attributes
		Book book = new Book(ISBN);
		book.setAuthor(author);
		book.setTitle(title);
		book.setNumberOfPages(numPages);
		
		System.out.println("Book information follows:");
		System.out.println("ISBN: " + book.getISBN());
		System.out.println("author: " + book.getAuthor());
		System.out.println("title: " + book.getTitle());
		System.out.println("number of pages: " + book.getNumberOfPages());
	}
}

package ch.fhnw.richards.week04_YourOwnClasses_Part2.person_v3;

import java.util.Scanner;

/**
 * This class is used to test the Person class.
 */
public class MainClass {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter first name and last name");
		String firstName = in.next();
		String lastName = in.next();
		System.out.println("Enter the person's gender (m or f)");
		String genderString = in.next(); // We will just look at the first character
		System.out.println("Enter the person's favorite color");
		String colorString = in.next();
		in.close();

		Person.Gender gender = Person.Gender.female;
		if (genderString.charAt(0) == 'm') gender = Person.Gender.male;
		
		Person.FavoriteColor color = Person.FavoriteColor.blue;
		if (colorString.equals("green")) color = Person.FavoriteColor.green;
		else if (colorString.equals("yellow")) color = Person.FavoriteColor.yellow;
		else if (colorString.equals("orange")) color = Person.FavoriteColor.orange;
		else if (colorString.equals("red")) color = Person.FavoriteColor.red;
		
		Person p = new Person(firstName, lastName);
		p.setGender(gender);
		p.setFavoriteColor(color);
		
		String description = p.toString();
		System.out.println(description);
	}

}

package ch.fhnw.richards.week13_Review.presidents;

import java.time.LocalDate;
import java.time.Period;

public class Term implements Comparable<Term> {
	// All attributes final, because this is history - it does not change
	private final LocalDate start;
	private final LocalDate end;
	private final TermEndReason reason;
	
	public Term(LocalDate start, LocalDate end, TermEndReason reason) {
		this.start = start;
		this.end = end;
		this.reason = reason;
	}
	
	/**
	 * The total length of this term, as a Period
	 */
	public Period lengthOfTerm() {
		return Period.between(start, end);
	}
	
	/**
	 * If we need to compare two terms, everything must be the same
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null || this.getClass() != o.getClass()) return false;
		Term t = (Term) o;
		return this.start.equals(t.start) && this.end.equals(t.end) && this.reason.equals(t.reason); 
	}

	/**
	 * We compare terms based on their starting dates. Since LocalDate implements
	 * Comparable, we can just use that result directly.
	 */
	@Override
	public int compareTo(Term o) {
		return this.start.compareTo(o.start);
	}
	
	// --- Getters ---
	
	public LocalDate getStart() {
		return start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public TermEndReason getReason() {
		return reason;
	}
}

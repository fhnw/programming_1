package ch.fhnw.richards.week13_Review.presidents;

import java.time.Period;
import java.util.ArrayList;

public class President {
	// Attributes are final, because history will not change. 
	private final String name;
	private final PoliticalParty party;
	private final ArrayList<Term> terms;

	public President(String name, PoliticalParty party) {
		this.name = name;
		this.party = party;
		this.terms = new ArrayList<>();
	}
	
	/**
	 * We can add terms to the list
	 */
	public void addTerm(Term term) {
		terms.add(term);
	}
	
	/**
	 * In this case, "name" is unique
	 */
	public boolean equals(Object o) {
		if (o == null || this.getClass() != o.getClass()) return false;
		President p = (President) o;
		return this.name.equals(p.name);
	}
	
	/**
	 * Add up the total length of all terms
	 */
	public Period timeInOffice() {
		Period totalTime = Period.of(0, 0, 0); // 0 years, 0 months, 0 days
		for (Term term : terms) {
			totalTime = totalTime.plus(term.lengthOfTerm());
		}
		return totalTime;
	}
	
	/**
	 * Return true if this person was assassinated
	 */
	public boolean assassinated() {
		boolean assassinated = false;
		for (Term term : terms) {
			assassinated |= (term.getReason() == TermEndReason.Assassinated);
		}
		return assassinated;
	}

	// --- Getters ---
	
	public String getName() {
		return name;
	}

	public PoliticalParty getParty() {
		return party;
	}

	public ArrayList<Term> getTerms() {
		return terms;
	}
}

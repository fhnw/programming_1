package ch.fhnw.richards.week13_Review.presidents;

public enum TermEndReason {
	None, Resigned, Died, Assassinated, Removed
}

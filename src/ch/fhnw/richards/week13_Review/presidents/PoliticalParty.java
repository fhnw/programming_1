package ch.fhnw.richards.week13_Review.presidents;

public enum PoliticalParty {
	Independent, Federalist, DemocraticRepublican, Democratic, Whig, NationalUnion, Republican
}

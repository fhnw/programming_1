package ch.fhnw.richards.week13_Review.presidents;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

public class TestPresident {

	public static void main(String[] args) {
		President georgeWashington = new President("George Washington", PoliticalParty.Independent);
		georgeWashington.addTerm(new Term(LocalDate.of(1789, 4, 30), LocalDate.of(1793, 3, 4), TermEndReason.None));
		georgeWashington.addTerm(new Term(LocalDate.of(1793, 3, 4), LocalDate.of(1797, 3, 4), TermEndReason.None));
		printInfo(georgeWashington);
		
		President abrahamLincoln = new President("Abraham Lincoln", PoliticalParty.Republican);
		abrahamLincoln.addTerm(new Term(LocalDate.of(1861, 3, 4), LocalDate.of(1865, 3, 4), TermEndReason.None));
		abrahamLincoln.addTerm(new Term(LocalDate.of(1865, 3, 4), LocalDate.of(1865, 4, 15), TermEndReason.Assassinated));
		printInfo(abrahamLincoln);
	}

	public static void printInfo(President pres) {
		System.out.print(pres.getName() + " was President for ");
		Period howLong = pres.timeInOffice();
		System.out.print(howLong.getYears() + " years, ");
		System.out.print(howLong.getMonths() + " months and ");
		System.out.println(howLong.getDays() + " days ");
		System.out.print(pres.getName() + " was ");
		if (!pres.assassinated()) System.out.print("not ");
		System.out.println("assassinated");
		System.out.println();
	}
}

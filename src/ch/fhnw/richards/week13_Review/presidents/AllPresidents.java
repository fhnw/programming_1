package ch.fhnw.richards.week13_Review.presidents;

import java.time.Period;
import java.util.ArrayList;

/**
 * A class to manage a list of all presidents.
 * Includes methods to calculate things from the entire collection
 */
public class AllPresidents {
	private final ArrayList<President> presidents;
	
	public AllPresidents() {
		presidents = new ArrayList<>();
	}

	public void add(President pres) {
		presidents.add(pres);
	}
	
	public President get(String name) {
		for (President p : presidents) {
			if (p.getName().equals(name)) return p;
		}
		return null; // not found
	}
	
	public Period termOfParty(PoliticalParty party) {
		Period total = Period.of(0,  0,  0);
		for (President p : presidents) {
			if (p.getParty().equals(party)) {
				total = total.plus(p.timeInOffice());
			}
		}
		return total;
	}
	
	public int numberOfParty(PoliticalParty party) {
		int total = 0;
		for (President p : presidents) {
			if (p.getParty().equals(party)) {
				total++;
			}
		}
		return total;
	}
}

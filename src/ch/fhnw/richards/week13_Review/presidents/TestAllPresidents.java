package ch.fhnw.richards.week13_Review.presidents;

import java.time.LocalDate;

import java.time.Period;

public class TestAllPresidents {
	public static void main(String[] args) {
		AllPresidents presidents = new AllPresidents();
		addData(presidents);
		
		printInfoOnParty(presidents, PoliticalParty.Republican);
		printInfoOnParty(presidents, PoliticalParty.Democratic);
	}
	
	private static void printInfoOnParty(AllPresidents presidents, PoliticalParty party) {
		int numPresidents = presidents.numberOfParty(party);
		Period totalTime = presidents.termOfParty(party);
		
		System.out.println("There have been " + numPresidents + " presidents");
		System.out.println("from the " + party + " party, for a total of");
		System.out.print(totalTime.getYears() + " years, " + totalTime.getMonths());
		System.out.println(" months and " + totalTime.getDays() + " days");	
		System.out.println();
	}

	private static void addData(AllPresidents ap) {
		President p = new President("George Washington", PoliticalParty.Independent);
		p.addTerm(new Term(LocalDate.of(1789, 4, 30), LocalDate.of(1793, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1793, 3, 4), LocalDate.of(1797, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("John Adams", PoliticalParty.Federalist);
		p.addTerm(new Term(LocalDate.of(1797, 3, 4), LocalDate.of(1801, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Thomas Jefferson", PoliticalParty.DemocraticRepublican);
		p.addTerm(new Term(LocalDate.of(1801, 3, 4), LocalDate.of(1805, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1805, 3, 4), LocalDate.of(1809, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("James Madison", PoliticalParty.DemocraticRepublican);
		p.addTerm(new Term(LocalDate.of(1809, 3, 4), LocalDate.of(1813, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1813, 3, 4), LocalDate.of(1817, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("JamesMonroe", PoliticalParty.DemocraticRepublican);
		p.addTerm(new Term(LocalDate.of(1817, 3, 4), LocalDate.of(1821, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1821, 3, 4), LocalDate.of(1825, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("John Quincy Adams", PoliticalParty.DemocraticRepublican);
		p.addTerm(new Term(LocalDate.of(1825, 3, 4), LocalDate.of(1829, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Andrew Jackson", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1829, 3, 4), LocalDate.of(1833, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1833, 3, 4), LocalDate.of(1837, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Martin Van Buren", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1837, 3, 4), LocalDate.of(1841, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("William Henry Harrison", PoliticalParty.Whig);
		p.addTerm(new Term(LocalDate.of(1841, 3, 4), LocalDate.of(1841, 4, 4), TermEndReason.Died));
		ap.add(p);

		p = new President("John Tyler", PoliticalParty.Whig);
		p.addTerm(new Term(LocalDate.of(1841, 4, 4), LocalDate.of(1845, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("James K. Polk", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1845, 3, 4), LocalDate.of(1849, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Zachary Taylor", PoliticalParty.Whig);
		p.addTerm(new Term(LocalDate.of(1849, 3, 4), LocalDate.of(1850, 7, 9), TermEndReason.Died));
		ap.add(p);

		p = new President("Millard Fillmore", PoliticalParty.Whig);
		p.addTerm(new Term(LocalDate.of(1850, 7, 9), LocalDate.of(1853, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Franklin Pierce", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1853, 3, 4), LocalDate.of(1857, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("James Buchanan", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1857, 3, 4), LocalDate.of(1861, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Abraham Lincoln", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1861, 3, 4), LocalDate.of(1865, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1865, 3, 4), LocalDate.of(1865, 4, 15), TermEndReason.Assassinated));
		ap.add(p);

		p = new President("Andrew Johnson", PoliticalParty.NationalUnion);
		p.addTerm(new Term(LocalDate.of(1865, 4, 15), LocalDate.of(1869, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Ulysses S. Grant", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1869, 3, 4), LocalDate.of(1873, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1873, 3, 4), LocalDate.of(1877, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Rutherford B. Hayes", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1877, 3, 4), LocalDate.of(1881, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("James A. Garfield", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1881, 3, 4), LocalDate.of(1881, 9, 19), TermEndReason.Assassinated));
		ap.add(p);

		p = new President("Chester A. Arthur", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1881, 9, 19), LocalDate.of(1885, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Grover Cleveland", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1885, 3, 4), LocalDate.of(1889, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1893, 3, 4), LocalDate.of(1897, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("Benjamin Harrison", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1889, 3, 4), LocalDate.of(1893, 3, 4), TermEndReason.None));
		ap.add(p);

		p = new President("William McKinley", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1897, 3, 4), LocalDate.of(1901, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1901, 3, 4), LocalDate.of(1901, 9, 14), TermEndReason.Assassinated));
		ap.add(p);
		
		p = new President("Theodore Roosevelt", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1901, 9, 14), LocalDate.of(1905, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1905, 3, 4), LocalDate.of(1909, 3, 4), TermEndReason.None));
		ap.add(p);
		
		p = new President("William Howard Taft", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1909, 3, 4), LocalDate.of(1913, 3, 4), TermEndReason.None));
		ap.add(p);
		
		p = new President("Woodrow Wilson", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1913, 3, 4), LocalDate.of(1917, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1917, 3, 4), LocalDate.of(1921, 3, 4), TermEndReason.None));
		ap.add(p);
		
		p = new President("Warren G. Harding", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1921, 3, 4), LocalDate.of(1923, 8, 2), TermEndReason.Died));
		ap.add(p);
		
		p = new President("Calvin Coolidge", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1923, 8, 2), LocalDate.of(1925, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1925, 3, 4), LocalDate.of(1929, 3, 4), TermEndReason.None));
		ap.add(p);
		
		p = new President("Herbert Hoover", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1929, 3, 4), LocalDate.of(1933, 3, 4), TermEndReason.None));
		ap.add(p);
		
		p = new President("Franklin D. Roosevelt", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1933, 3, 4), LocalDate.of(1937, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1937, 3, 4), LocalDate.of(1941, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1941, 3, 4), LocalDate.of(1945, 3, 4), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1945, 3, 4), LocalDate.of(1945, 4, 12), TermEndReason.None));
		ap.add(p);
		
		p = new President("Harry S. Truman", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1945, 4, 12), LocalDate.of(1953, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("Dwight D. Eisenhower", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1953, 1, 20), LocalDate.of(1957, 1, 20), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1957, 1, 20), LocalDate.of(1961, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("John F. Kennedy", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1961, 1, 20), LocalDate.of(1963, 11, 22), TermEndReason.Assassinated));
		ap.add(p);
		
		p = new President("Lyndon B. Johnson", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1963, 11, 22), LocalDate.of(1965, 1, 20), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1965, 1, 20), LocalDate.of(1969, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("Richard Nixon", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1969, 1, 20), LocalDate.of(1973, 1, 20), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1973, 1, 20), LocalDate.of(1974, 8, 9), TermEndReason.Resigned));
		ap.add(p);
		
		p = new President("Gerald Ford", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1974, 8, 9), LocalDate.of(1977, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("Jimmy Carter", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1977, 1, 20), LocalDate.of(1981, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("Ronald Reagan", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1981, 1, 20), LocalDate.of(1985, 1, 20), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1985, 1, 20), LocalDate.of(1989, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("George H. W. Bush", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(1989, 1, 20), LocalDate.of(1993, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("Bill Clinton", PoliticalParty.Democratic);
		p.addTerm(new Term(LocalDate.of(1993, 1, 20), LocalDate.of(1997, 1, 20), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(1997, 1, 20), LocalDate.of(2001, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("George W. Bush", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(2001, 1, 20), LocalDate.of(2005, 1, 20), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(2005, 1, 20), LocalDate.of(2009, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("Barack Obama", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(2009, 1, 20), LocalDate.of(2013, 1, 20), TermEndReason.None));
		p.addTerm(new Term(LocalDate.of(2013, 1, 20), LocalDate.of(2017, 1, 20), TermEndReason.None));
		ap.add(p);
		
		p = new President("Donald Trump", PoliticalParty.Republican);
		p.addTerm(new Term(LocalDate.of(2017, 1, 20), LocalDate.of(2021, 1, 20), TermEndReason.None));
		ap.add(p);
	}
}

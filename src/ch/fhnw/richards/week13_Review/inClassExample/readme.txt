Advent of Code 2020, Day 2, Part 1

Start with a class that can read the lines from the input file.
The main method calls a second method to check each line.

The task in-class is to write this method.
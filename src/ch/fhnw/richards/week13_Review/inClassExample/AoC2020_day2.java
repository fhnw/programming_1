package ch.fhnw.richards.week13_Review.inClassExample;

import java.util.ArrayList;
import java.util.Scanner;

public class AoC2020_day2 {

	public static void main(String[] args) {
		Scanner in = new Scanner(AoC2020_day2.class.getResourceAsStream("input.txt"));
		ArrayList<String> lines = new ArrayList<>();
		while (in.hasNext()) lines.add(in.nextLine());
		
		int count = 0;
		for (String line : lines) {
			if (checkLine(line)) count++;
		}
		
		System.out.println(count);
	}

	private static boolean checkLine(String line) {
		// First, parse out the important pieces of data
		String[] parts = line.split(" ");
		String[] parts2 = parts[0].split("-");
		int least = Integer.parseInt(parts2[0]);
		int most = Integer.parseInt(parts2[1]);
		char letter = parts[1].charAt(0);
		String password = parts[2];
		
		// Count occurrences of c in password
		int count = 0;
		for (char c : password.toCharArray()) {
			if (c == letter) count++;
		}
		
		return (count >= least && count <= most);
	}
}

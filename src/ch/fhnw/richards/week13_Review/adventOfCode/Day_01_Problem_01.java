package ch.fhnw.richards.week13_Review.adventOfCode;

import java.util.ArrayList;
import java.util.Scanner;

public class Day_01_Problem_01 {

	public static void main(String[] args) {
		// Read input file
		ArrayList<Integer> values = readInput();
		
		// Check all possible pairs of numbers, until we find a solution
		boolean found = false;
		for (int i = 0; !found && i < values.size() - 1; i++) {
			Integer value1 = values.get(i);
			for (int j = i+1; !found && j < values.size(); j++) {
				Integer value2 = values.get(j);
				
				// Is this a solution?
				if ( (value1 + value2) == 2020) {
					System.out.println("Answer = " + (value1 * value2));
					found = true; // End the loops
				}
			}
		}
	}

	private static ArrayList<Integer> readInput() {
		ArrayList<Integer> inputs = new ArrayList<>();
		Scanner in = new Scanner(Day_01_Problem_01.class.getResourceAsStream("input.txt"));
		while(in.hasNext()) {
			Integer input = Integer.parseInt(in.next());
			inputs.add(input);
		}
		in.close();
		return inputs;
	}
}

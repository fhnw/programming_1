package ch.fhnw.richards.week13_Review;

public class Assignments {

	public static void main(String[] args) {
		int a = 2 + 3 * 4;
		System.out.println(a);
		
		int b = (2 + 3) * 4;
		System.out.println(b);

		int c = 22;
		boolean b1 = c < 100;
		System.out.println(b1);
		
		int d = 22;
		boolean b2 = (d < 100) && (d % 2 == 0);
		System.out.println(b2);

		String s = "asdf";
		boolean b3 = (s != null) && (s.charAt(1) < 'm');
		System.out.println(b3);
	}

}

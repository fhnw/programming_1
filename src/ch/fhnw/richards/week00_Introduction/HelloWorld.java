package ch.fhnw.richards.week00_Introduction;

public class HelloWorld {
	/**
	 * The main method is where the program starts.
	 * 
	 * For the moment, we will ignore what is in the parentheses
	 */
	public static void main(String[] args) {
		// Say "hello" to the user
		System.out.println("Hello, World");
	}

}

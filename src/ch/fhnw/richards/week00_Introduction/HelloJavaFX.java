package ch.fhnw.richards.week00_Introduction;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class HelloJavaFX extends Application {

  public static void main(String[] args) {
    launch();
  }

  @Override
  public void start(Stage stage) throws Exception {
    BorderPane root = new BorderPane();
    root.setCenter(new Label("Hello, JavaFX"));
    Scene scene = new Scene(root);
    stage.setTitle("Hello, JavaFX");
    stage.setScene(scene);
    stage.show();
  }
}

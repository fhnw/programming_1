package ch.fhnw.richards.week09_Comparing.inClassExample;

import java.util.Objects;

public class Civilization implements Comparable<Civilization> {
	private final String name;
	private final String story;
	private int advanced;
	
	public Civilization(String name, String story) {
		this.name = name;
		this.story = story;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if ( ! this.getClass().equals(o.getClass()) ) return false;
		Civilization c = (Civilization) o;
		return this.name.equals(c.name) && this.story.equals(c.story);
	}

	@Override
	public int compareTo(Civilization c) {
		int order = this.name.compareTo(c.name);
		if (order == 0) order = this.story.compareTo(c.story);
		return order;
	}

	public int getAdvanced() {
		return advanced;
	}

	public void setAdvanced(int advanced) {
		this.advanced = advanced;
	}

	public String getName() {
		return name;
	}

	public String getStory() {
		return story;
	}
	
	
	
}

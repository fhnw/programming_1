In the realm of science fiction, there are a lot of alien species and civilizations.
We want to have the name of the civilization, which storye they came from
(franchise, book, movie, etc.), and a numerical rating of their technological level
(0 = primitive, 10 = wow).

Look at implementing ID as a number, and then as a combination of name and story.

Then think about a natural ordering. Using technology level is easy to implement,
but not consistent with equals. Using name and then story is consistent, giving
an alphabetical ordering.

Sample data:

Vulcan - Star Trek - 5
Klingon - Star Trek - 4
Wookie - Star Wars - 3
Asari - Mass Effect - 7
Eldar - Warhammer 40000 - 8

Task:

Create a class Civilization as described

Create a main class to:
- Read in sample data
- Store it in an ArrayList
- Sort it
- Print the data out in sorted order

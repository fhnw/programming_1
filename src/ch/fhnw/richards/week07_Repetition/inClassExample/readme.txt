Enter several high mountain peaks in Switzerland
-- Store these as objects of the data class Mountain

Implement a method to sort the peaks by their altitude
-- Use selection sort

Print the peaks, from highest to lowest

-----

Some data to enter

Aletschhorn 4193
Castor 4228
Dom 4545
Matterhorn 4478
Weisshorn 4506

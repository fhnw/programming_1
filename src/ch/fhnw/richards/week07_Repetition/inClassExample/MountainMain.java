package ch.fhnw.richards.week07_Repetition.inClassExample;

import java.util.ArrayList;
import java.util.Scanner;

public class MountainMain {

	public static void main(String[] args) {
		ArrayList<Mountain> mountains = new ArrayList<>();
		
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the name of a mountain and its height (m)");
		System.out.println("Enter a height of 0 to stop");
		
		String name = in.next();
		int height = in.nextInt();
		while (height > 0) {
			Mountain m = new Mountain(name, height);
			mountains.add(m);
			name = in.next();
			height = in.nextInt();
		}
		in.close();
		
		sortByHeight(mountains);
		
		int position = 0;
		while (position < mountains.size()) {
			System.out.println(mountains.get(position));
			position++;
		}
	}

	private static void sortByHeight(ArrayList<Mountain> mountains) {
		// For all positions in the list
		for (int i = 0; i < mountains.size(); i++) {
			int highest = i; // Assume position i is the highest
			// Now look through all the rest, for the actual highest mountains
			for (int j = i+i; j < mountains.size(); j++) {
				if (mountains.get(j).getHeight() > mountains.get(highest).getHeight()) {
					highest = j; // Found a higher mountain, so save its position
				}
			}
			// Swap the highest mountain to position i in the list
			if (i != highest) swap(mountains, i, highest);
		}
	}
	
	private static void swap(ArrayList<Mountain> mountains, int x, int y) {
		Mountain temp = mountains.get(x);
		mountains.set(x, mountains.get(y));
		mountains.set(y, temp);
	}
}

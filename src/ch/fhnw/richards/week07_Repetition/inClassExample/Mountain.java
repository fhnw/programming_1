package ch.fhnw.richards.week07_Repetition.inClassExample;

public class Mountain {
	private final String name;
	private int height;
	
	public Mountain(String name, int height) {
		this.name = name;
		this.height = height;
	}
	
	@Override
	public String toString() {
		return name + " is " + height + " meters high";
	}

	public String getName() {
		return name;
	}

	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		if (height > 0) this.height = height;
	}
}

package ch.fhnw.richards.week07_Repetition.exampleKattis;

import java.util.Scanner;

public class autori {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		String answer = "";
		int pos = 0;
		while (pos < s.length()) {
			char c = s.charAt(pos);
			if (Character.isUpperCase(c)) answer += c;
			pos++;
		}
		System.out.println(answer);
	}

}

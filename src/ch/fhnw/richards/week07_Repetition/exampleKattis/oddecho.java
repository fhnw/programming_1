package ch.fhnw.richards.week07_Repetition.exampleKattis;

import java.util.Scanner;

public class oddecho {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int number = in.nextInt();
		
		int count = 1;
		while (count <= number) {
			String word = in.next();
			if (count % 2 != 0) System.out.println(word);
			
			count++;
		}
		in.close();
	}

}

package ch.fhnw.richards.week10_Arrays.inClassExample;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This solution will result in "time limit exceeded" for later test cases.
 * That is ok! Efficiency is not the point for us.
 */
public class keywords {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		ArrayList<String> words = new ArrayList<>();
		int numWords = in.nextInt(); in.nextLine();
		for (int i = 0; i < numWords; i++) {
			String newWord = in.nextLine();       // Read next word
			newWord = newWord.toLowerCase();      // Ignore case
			
			// Replace spaces with dashes
			char[] chars = newWord.toCharArray();
			for (int j = 0; j < chars.length; j++) {
				if (chars[j] == ' ') chars[j] = '-';				
			}
			newWord = new String(chars); // Our final new word
			
			// Add to our list only if not already present
			boolean found = false;
			for (int j = 0; j < words.size(); j++) {
				if (words.get(j).equals(newWord)) found = true;
			}			
			if (!found) words.add(newWord);
		}
		// How many words do we have?
		System.out.println(words.size());
	}
}

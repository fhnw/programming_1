package ch.fhnw.richards.week02_BasicInputOutput;

public class Boolean_example {

	public static void main(String[] args) {
		int x = 3;
		int y = 87;
		int z = -23;
		
		boolean x_largest = x > y && x > z;
		boolean y_largest = y > x && y > z;
		boolean z_largest = z > x && z > y;

		if (x_largest) System.out.println("X is the largest");
		else if (y_largest) System.out.println("Y is the largest");
		else System.out.println("Z is the largest");
	}

}
